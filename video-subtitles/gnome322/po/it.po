# Italian translation for video-subtitles.
# Copyright (C) 2016 video-subtitles's COPYRIGHT HOLDER
# This file is distributed under the same license as the video-subtitles package.
# Gianvito Cavasoli <gianvito@gmx.it>, 2016.
#
#. extracted from ../gnome322.srt
msgid ""
msgstr ""
"Project-Id-Version: video-subtitles master\n"
"Report-Msgid-Bugs-To: \n"
"POT-Creation-Date: 2016-09-21 22:23+0000\n"
"PO-Revision-Date: 2016-09-22 15:40+0200\n"
"Last-Translator: Gianvito Cavasoli <gianvito@gmx.it>\n"
"Language-Team: Italiano <gnome-it-list@gnome.org>\n"
"Language: it\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"X-Generator: Gtranslator 2.91.7\n"
"Plural-Forms: nplurals=2; plural=(n != 1);\n"

#. visible for 3 seconds
#: 00:00:03.380-->00:00:06.440
msgid "GNOME is developing a free desktop for the next generation"
msgstr "GNOME sta sviluppando un desktop libero per la prossima generazione"

#. visible for 1 seconds
#: 00:00:07.080-->00:00:08.820
msgid "It's created by an open community"
msgstr "È creato da una comunità aperta"

#. visible for 2 seconds
#: 00:00:08.820-->00:00:11.780
msgid ", which provides a simple and beautiful experience for everyone."
msgstr ", che fornisce un'esperienza semplice e bella per tutti."

#. visible for 2 seconds
#: 00:00:13.980-->00:00:16.240
msgid "The application core is continually being improved."
msgstr "Le applicazioni cardine vengono continuamente migliorate."

#. visible for 3 seconds
#: 00:00:17.220-->00:00:20.500
msgid ""
"Many of the advancements were made by new and long-time contributors alike,"
msgstr ""
"Molti dei progressi sono stati fatti al contempo da collaboratori nuovi e di "
"lunga data,"

#. visible for 3 seconds
#: 00:00:20.500-->00:00:24.020
msgid "..together with GNOME interns from Outreachy and Google Summer of Code."
msgstr "..insieme a praticanti di GNOME da Outreachy e Google Summer of Code."

#. visible for 2 seconds
#: 00:00:26.300-->00:00:28.820
msgid "Files has seen a lot of new development this cycle."
msgstr "«File» ha visto molti nuovi sviluppi in questo ciclo."

#. visible for 2 seconds
#: 00:00:31.060-->00:00:33.460
msgid "Archives are now automatically extracted when opened."
msgstr "Gli archivi ora sono automaticamente estratti quando vengono aperti."

#. visible for 3 seconds
#: 00:00:37.880-->00:00:41.500
msgid ""
"Files can also compress your data to the most commonly used archive formats.."
msgstr ""
"«File» può anche comprimere i dati nei formati di archivio più comunemente "
"usati.."

#. visible for 2 seconds
#: 00:00:41.500-->00:00:43.940
msgid "..such as zip, tar and 7-zip."
msgstr "..come zip, tar e 7-zip."

#. visible for 5 seconds
#: 00:00:45.940-->00:00:51.180
msgid ""
"Moreover bulk renaming support makes it possible to rename multiple files at "
"once in a single operation."
msgstr ""
"In aggiunta il supporto di ridenominazione di massa rende possibile "
"rinominare più file allo stesso tempo in una sola operazione."

#. visible for 3 seconds
#: 00:00:52.300-->00:00:55.780
msgid "A variety of patterns can be used such as numbers or file metadata."
msgstr ""
"Una varietà di modelli possono essere usati come numeri o metadati del file."

#. visible for 3 seconds
#: 00:00:56.880-->00:00:59.880
msgid "This makes organizing large collections of files easy and convenient."
msgstr ""
"Questo rende l'organizzazione di grandi raccolte di file facile e comoda."

#. visible for 3 seconds
#: 00:01:04.900-->00:01:08.360
msgid ""
"GNOME Software has received visual improvements and flatpak integration."
msgstr ""
"GNOME Software ha ricevuto miglioramenti visivi e l'integrazione a flatpak."

#. visible for 2 seconds
#: 00:01:09.940-->00:01:12.780
msgid "The landing page has been polished to be more attractive and colorful."
msgstr ""
"La finestra principale è stata raffinata per essere più attraente e colorata."

#. visible for 4 seconds
#: 00:01:13.300-->00:01:17.920
msgid ""
"This cycle also lands further integration for installing Flatpak bundles and "
"repositories."
msgstr ""
"Questo ciclo riceve anche un ulteriore integrazione per l'installazione di "
"gruppi di pacchetti e repository di Flatpak."

#. visible for 0 seconds
#: 00:01:21.660-->00:01:22.620
msgid "Furthermore,"
msgstr "Inoltre,"

#. visible for 3 seconds
#: 00:01:23.700-->00:01:26.740
msgid "GNOME Photos can share images from your image collection."
msgstr "GNOME Foto può condividere le immagini dalla tua collezione."

#. visible for 2 seconds
#: 00:01:27.620-->00:01:30.480
msgid "Currently sharing is supported by e-mail and via Google Photos."
msgstr "Attualmente la condivisione è supportata via e-mail e Google Foto."

#. visible for 2 seconds
#: 00:01:33.760-->00:01:36.680
msgid "Calendar supports using drag n' drop to move your calendar events,.."
msgstr ""
"Calendario supporta l'uso del «trascina e rilascia» per spostare gli "
"eventi,.."

#. visible for 2 seconds
#: 00:01:36.680-->00:01:38.680
msgid "..making event rescheduling a breeze."
msgstr "..rendendo la riprogrammazione di un evento un gioco da ragazzi."

#. visible for 2 seconds
#: 00:01:40.320-->00:01:42.880
msgid "Polari includes support for remembering passwords.."
msgstr "Polari include il supporto del ricordo della password.."

#. visible for 2 seconds
#: 00:01:42.880-->00:01:45.260
msgid "..when identifying with authentication services."
msgstr "..quando si identifica con i servizi di autenticazione."

#. visible for 2 seconds
#: 00:01:46.540-->00:01:49.280
msgid "..and GNOME Games has received gamepad support."
msgstr "..e GNOME Giochi ha ricevuto il supporto al gamepad."

#. visible for 3 seconds
#: 00:01:51.860-->00:01:55.160
msgid "Major work is being undertaken to improve the desktop infrastructure."
msgstr ""
"il lavoro maggiore è stato intrapreso per migliorare l'infrastruttura "
"desktop."

#. visible for 2 seconds
#: 00:01:55.200-->00:01:57.460
msgid "GNOME is enhancing desktop security.."
msgstr "GNOME ha migliorato la sicurezza del desktop.."

#. visible for 4 seconds
#: 00:01:57.460-->00:02:01.820
msgid "..by building sandboxing technology with GTK, Wayland, and Flatpak."
msgstr ""
"..con la tecnologia di costruzione di un ambiente di prova con GTK, Wayland "
"e Flatpak."

#. visible for 5 seconds
#: 00:02:03.420-->00:02:08.480
msgid ""
"With this release, developers can make use of sandboxing technology directly "
"from GNOME Builder."
msgstr ""
"Con questo rilascio, gli sviluppatori possono usare la tecnologia di un "
"ambiente di prova direttamente da GNOME Builder."

#. visible for 4 seconds
#: 00:02:09.480-->00:02:13.660
msgid ""
"From the revamped headerbar, you can compile your projects against a Flatpak "
"runtime."
msgstr ""
"Dalla rinnovata barra di intestazione, si possono compilare i progetti a "
"favore di un eseguibile Flatpak."

#. visible for 3 seconds
#: 00:02:17.460-->00:02:21.120
msgid "GNOME 3.22 will be available through many distributions soon."
msgstr "GNOME 3.22 sarà presto disponibile tramite molte distribuzioni."

#. visible for 1 seconds
#: 00:02:21.240-->00:02:22.520
msgid "We hope you like it!"
msgstr "Speriamo che vi piaccia!"

#. visible for 2 seconds
#: 00:02:23.060-->00:02:25.900
msgid "The GNOME community is friendly and open for anyone."
msgstr "La comunità di GNOME è amichevole e aperta a chiunque."

#. visible for 4 seconds
#: 00:02:26.080-->00:02:30.340
msgid ""
"We invite you to help us push the free desktop further by getting involved "
"today."
msgstr ""
"Vi invitiamo ad aiutarci a spingere ulteriormente il desktop libero "
"attirando oggi la vostra attenzione."
