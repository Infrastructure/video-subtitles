1
00:00:03,380 --> 00:00:06,440
GNOME is developing a free desktop for the next generation

2
00:00:07,080 --> 00:00:08,820
It's created by an open community

3
00:00:08,820 --> 00:00:11,780
, which provides a simple and beautiful experience for everyone.

4
00:00:13,980 --> 00:00:16,240
The application core is continually being improved.

5
00:00:17,220 --> 00:00:20,500
Many of the advancements were made by new and long-time contributors alike,

6
00:00:20,500 --> 00:00:24,020
..together with GNOME interns from Outreachy and Google Summer of Code.

7
00:00:26,300 --> 00:00:28,820
Files has seen a lot of new development this cycle.

8
00:00:31,060 --> 00:00:33,460
Archives are now automatically extracted when opened.

9
00:00:37,880 --> 00:00:41,500
Files can also compress your data to the most commonly used archive formats..

10
00:00:41,500 --> 00:00:43,940
..such as zip, tar and 7-zip.

11
00:00:45,940 --> 00:00:51,180
Moreover bulk renaming support makes it possible to rename multiple files at once in a single operation.

12
00:00:52,300 --> 00:00:55,780
A variety of patterns can be used such as numbers or file metadata.

13
00:00:56,880 --> 00:00:59,880
This makes organizing large collections of files easy and convenient.

14
00:01:04,900 --> 00:01:08,360
GNOME Software has received visual improvements and flatpak integration.

15
00:01:09,940 --> 00:01:12,780
The landing page has been polished to be more attractive and colorful.

16
00:01:13,300 --> 00:01:17,920
This cycle also lands further integration for installing Flatpak bundles and repositories.

17
00:01:21,660 --> 00:01:22,620
Furthermore,

18
00:01:23,700 --> 00:01:26,740
GNOME Photos can share images from your image collection.

19
00:01:27,620 --> 00:01:30,480
Currently sharing is supported by e-mail and via Google Photos.

20
00:01:33,760 --> 00:01:36,680
Calendar supports using drag n' drop to move your calendar events,..

21
00:01:36,680 --> 00:01:38,680
..making event rescheduling a breeze.

22
00:01:40,320 --> 00:01:42,880
Polari includes support for remembering passwords..

23
00:01:42,880 --> 00:01:45,260
..when identifying with authentication services.

24
00:01:46,540 --> 00:01:49,280
..and GNOME Games has received gamepad support.

25
00:01:51,860 --> 00:01:55,160
Major work is being undertaken to improve the desktop infrastructure.

26
00:01:55,200 --> 00:01:57,460
GNOME is enhancing desktop security..

27
00:01:57,460 --> 00:02:01,820
..by building sandboxing technology with GTK, Wayland, and Flatpak.

28
00:02:03,420 --> 00:02:08,480
With this release, developers can make use of sandboxing technology directly from GNOME Builder.

29
00:02:09,480 --> 00:02:13,660
From the revamped headerbar, you can compile your projects against a Flatpak runtime.

30
00:02:17,460 --> 00:02:21,120
GNOME 3.22 will be available through many distributions soon.

31
00:02:21,240 --> 00:02:22,520
We hope you like it!

32
00:02:23,060 --> 00:02:25,900
The GNOME community is friendly and open for anyone.

33
00:02:26,080 --> 00:02:30,340
We invite you to help us push the free desktop further by getting involved today.

