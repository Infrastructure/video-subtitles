1
00:00:01,840 --> 00:00:06,074
Auf der Nordhalbkugel ist der Frühling eingetroffen, und ebenso eine neue Version von GNOME!

2
00:00:06,074 --> 00:00:12,094
In dieser Version liefern wir angepasste Themen und eine bessere Entwicklungsumgebung

3
00:00:13,060 --> 00:00:17,220
Mehrere Anwendungen sind umgestaltet worden, wie zum Beispiel der Bildbetrachter von GNOME.

4
00:00:17,220 --> 00:00:20,340
… der ab sofort neue Bedienelemente besitzt.

5
00:00:26,075 --> 00:00:28,475
»Dateien« hat ebenfalls Aufmerksamkeit erhalten …

6
00:00:28,494 --> 00:00:30,894
… mit vereinfachten Überblendmenüs …

7
00:00:30,894 --> 00:00:32,994
… und einer verbesserten Dateiansicht.

8
00:00:36,280 --> 00:00:40,240
GNOME 3.16 bringt auch neue Grafiken für die GNOME Shell.

9
00:00:43,240 --> 00:00:45,280
Dies betrifft die Aktivitäten-Übersicht …

10
00:00:45,740 --> 00:00:47,940
… das Anwendungsmenü …

11
00:00:48,060 --> 00:00:50,200
… und das Systemmenü.

12
00:00:52,740 --> 00:00:57,700
GNOME Shell also provides an improved design of the notification system.

13
00:00:59,540 --> 00:01:03,880
These improvements aim to make notifications in GNOME more discoverable..

14
00:01:03,920 --> 00:01:06,600
..but also easy to get out of your way.

15
00:01:09,380 --> 00:01:12,540
This cycle features three new GNOME apps.

16
00:01:12,800 --> 00:01:15,600
One of them is a preview of GNOME Calendar..

17
00:01:15,800 --> 00:01:18,660
..an app which helps you plan ahead.

18
00:01:26,540 --> 00:01:29,000
GNOME Calendar integrates with your online accounts..

19
00:01:29,260 --> 00:01:31,960
..so you can take your calendar with you on the go.

20
00:01:32,820 --> 00:01:35,860
This release also features a preview of GNOME books..

21
00:01:35,980 --> 00:01:38,980
..a new application for your book-reading needs.

22
00:01:42,860 --> 00:01:45,420
Furthermore, this release brings a preview of Builder..

23
00:01:45,560 --> 00:01:47,407
… eine neue Anwendung für Entwickler.

24
00:01:48,580 --> 00:01:52,360
Builder is an IDE aiming to improve the developer experience on GNOME.

25
00:01:55,360 --> 00:01:59,520
Thanks to more than 500 funders the project was succesfully crowd-funded.

26
00:02:00,160 --> 00:02:03,180
If you want to help improve the developer experience too,..

27
00:02:03,300 --> 00:02:06,420
..visit the crowdfunding campaign in the description below.

28
00:02:08,540 --> 00:02:12,240
GTK+ also aims to improve the development experience this cycle,..

29
00:02:13,400 --> 00:02:15,780
..with many improvements to the GtkInspector,..

30
00:02:15,780 --> 00:02:17,620
… Unterstützung für OpenGL …

31
00:02:18,280 --> 00:02:19,634
..and a mir backend.

32
00:02:20,720 --> 00:02:23,860
Finally, GNOME 3.16 ships an improved glib,..

33
00:02:24,140 --> 00:02:27,200
..aiming to make life much easier for C developers.

34
00:02:32,340 --> 00:02:36,180
The new GNOME release is available for you as a live image to try now.

35
00:02:37,420 --> 00:02:41,320
GNOME 3.16 will be shipped by many distributions in the near future.

36
00:02:43,680 --> 00:02:45,660
GNOME is made by people for people.

37
00:02:45,740 --> 00:02:49,260
Help us make GNOME better by getting involved today.
