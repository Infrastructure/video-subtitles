1
00:00:01,840 --> 00:00:06,074
Le printemps est arrivé dans l'hémisphère nord, c'est le moment pour une nouvelle version de GNOME !

2
00:00:06,074 --> 00:00:12,094
Dans cette version, nous vous proposons un système de thèmes simplifié et une meilleure expérience pour les développeurs.

3
00:00:13,060 --> 00:00:17,220
Plusieurs applications ont été retravaillées, par exemple le visionneur d'images de GNOME.

4
00:00:17,220 --> 00:00:20,340
qui comprend désormais de nouveaux contrôles.

5
00:00:26,075 --> 00:00:28,475
Fichiers a aussi été amélioré

6
00:00:28,494 --> 00:00:30,894
avec des menus surgissants simplifiés

7
00:00:30,894 --> 00:00:32,994
et une meilleure vue des fichiers.

8
00:00:36,280 --> 00:00:40,240
Dans GNOME 3.16, le Shell a un nouveau thème

9
00:00:43,240 --> 00:00:45,280
pour la vue d'ensemble des activités,

10
00:00:45,740 --> 00:00:47,940
le menu d'application

11
00:00:48,060 --> 00:00:50,200
et le menu système.

12
00:00:52,740 --> 00:00:57,700
GNOME Shell fournit maintenant un nouveau système de notifications.

13
00:00:59,540 --> 00:01:03,880
Cette amélioration vise à rendre les notifications plus faciles à trouver

14
00:01:03,920 --> 00:01:06,600
mais aussi plus faciles à faire disparaître.

15
00:01:09,380 --> 00:01:12,540
Trois nouvelles applications sont apparues durant ce cycle.

16
00:01:12,800 --> 00:01:15,600
L'une d'elle est une pré-version de l'Agenda GNOME,

17
00:01:15,800 --> 00:01:18,660
une application pour vous aider à planifier.

18
00:01:26,540 --> 00:01:29,000
Elle s'intègre avec vos comptes en ligne

19
00:01:29,260 --> 00:01:31,960
pour que vous puissiez consultez vos agendas où que vous soyez.

20
00:01:32,820 --> 00:01:35,860
Cette version intègre aussi une pré-version de GNOME Livres,

21
00:01:35,980 --> 00:01:38,980
une nouvelle application pour vos besoins en lecture.

22
00:01:42,860 --> 00:01:45,420
De plus, cette version amène une pré-version de Builder,

23
00:01:45,560 --> 00:01:47,407
une nouvelle application pour les développeurs.

24
00:01:48,580 --> 00:01:52,360
Builder est un EDI visant à améliorer l'expérience des développeurs dans GNOME.

25
00:01:55,360 --> 00:01:59,520
Grâce à plus de 500 participants, le projet a été financé avec succès.

26
00:02:00,160 --> 00:02:03,180
Si vous voulez aider à améliorer l'expérience développeurs,

27
00:02:03,300 --> 00:02:06,420
visitez la page de la campagne de financement participatif dans la description ci-dessous.

28
00:02:08,540 --> 00:02:12,240
GTK+ vise également à améliorer l'expérience développeurs ce cycle,

29
00:02:13,400 --> 00:02:15,780
avec de nombreux changements dans GtkInspector,

30
00:02:15,780 --> 00:02:17,620
la prise en charge d'OpenGL

31
00:02:18,280 --> 00:02:19,634
et un moteur Mir.

32
00:02:20,720 --> 00:02:23,860
Enfin, GNOME 3.16 contient une version améliorée de glib,

33
00:02:24,140 --> 00:02:27,200
cherchant à simplifier la vie des développeurs C.

34
00:02:32,340 --> 00:02:36,180
La nouvelle version de GNOME est disponible sous la forme d'une image live.

35
00:02:37,420 --> 00:02:41,320
GNOME 3.16 sera dans les dépôts de nombreuses distributions dans un avenir proche.

36
00:02:43,680 --> 00:02:45,660
GNOME est fait par la communauté, pour tous.

37
00:02:45,740 --> 00:02:49,260
Aidez-nous à rendre GNOME meilleur en vous impliquant aujourd'hui.
