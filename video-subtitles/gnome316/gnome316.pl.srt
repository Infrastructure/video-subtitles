1
00:00:01,840 --> 00:00:06,074
Wraz z nadejściem wiosny witamy nowe wydanie GNOME.

2
00:00:06,074 --> 00:00:12,094
Zawiera ono usprawniony motyw wizualny i ulepszenia dla programistów.

3
00:00:13,060 --> 00:00:17,220
Niektóre programy zostały zmodernizowane, w tym przeglądarka obrazów GNOME,

4
00:00:17,220 --> 00:00:20,340
która zawiera teraz nowy interfejs użytkownika.

5
00:00:26,075 --> 00:00:28,475
Menedżer plików także został odświeżony

6
00:00:28,494 --> 00:00:30,894
dzięki uproszczonym menu

7
00:00:30,894 --> 00:00:32,994
i ulepszonym widokiem plików.

8
00:00:36,280 --> 00:00:40,240
GNOME 3.16 zawiera także nowy motyw graficzny w Powłoce GNOME.

9
00:00:43,240 --> 00:00:45,280
Zmieniono ekran podglądu,

10
00:00:45,740 --> 00:00:47,940
menu programu

11
00:00:48,060 --> 00:00:50,200
i menu systemowe.

12
00:00:52,740 --> 00:00:57,700
W Powłoce GNOME przeprojektowano także system powiadomień.

13
00:00:59,540 --> 00:01:03,880
Dzięki temu powiadomienia w GNOME są łatwiejsze do wyświetlenia,

14
00:01:03,920 --> 00:01:06,600
ale również mniej przeszkadzają w pracy.

15
00:01:09,380 --> 00:01:12,540
W tym cyklu wydawniczym dodano trzy nowe programy GNOME.

16
00:01:12,800 --> 00:01:15,600
Jednym z nich jest wersja beta Kalendarza GNOME,

17
00:01:15,800 --> 00:01:18,660
który pomaga w planowaniu wydarzeń.

18
00:01:26,540 --> 00:01:29,000
Kalendarz GNOME integruje się z kontami online,

19
00:01:29,260 --> 00:01:31,960
więc możesz wszędzie go ze sobą zabrać.

20
00:01:32,820 --> 00:01:35,860
To wydanie zawiera także wersję beta Czytnika książek GNOME,

21
00:01:35,980 --> 00:01:38,980
programu przeznaczonego do czytania e-booków.

22
00:01:42,860 --> 00:01:45,420
Co więcej, w tym wydaniu dostępna jest wersja beta programu Builder

23
00:01:45,560 --> 00:01:47,407
przeznaczonego dla programistów.

24
00:01:48,580 --> 00:01:52,360
Builder to zintegrowane środowisko programistyczne ułatwiające pracę w GNOME.

25
00:01:55,360 --> 00:01:59,520
Ten projekt został wsparty finansowo przez ponad 500 osób.

26
00:02:00,160 --> 00:02:03,180
Można pomóc ulepszyć interfejs GNOME dla programistów,

27
00:02:03,300 --> 00:02:06,420
odwiedzając kampanię finansowania społecznego (odnośnik w opisie).

28
00:02:08,540 --> 00:02:12,240
W tym cyklu ulepszono także bibliotekę GTK+ dla programistów

29
00:02:13,400 --> 00:02:15,780
dzięki wielu zmianom w Inspektorze biblioteki GTK+,

30
00:02:15,780 --> 00:02:17,620
obsłudze OpenGL

31
00:02:18,280 --> 00:02:19,634
i mechanizmu Mir.

32
00:02:20,720 --> 00:02:23,860
GNOME 3.16 zawiera także ulepszoną bibliotekę GLib

33
00:02:24,140 --> 00:02:27,200
ułatwiającą życie programistom języka C.

34
00:02:32,340 --> 00:02:36,180
Nowe wydanie GNOME jest dostępne jako obraz Live do wypróbowania.

35
00:02:37,420 --> 00:02:41,320
W przyszłości wiele dystrybucji będzie zawierało GNOME 3.16.

36
00:02:43,680 --> 00:02:45,660
GNOME jest tworzone przez ludzi, dla ludzi.

37
00:02:45,740 --> 00:02:49,260
Pomóż nam je ulepszyć!
