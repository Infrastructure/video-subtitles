1
00:00:01,840 --> 00:00:06,074
A primavera chegou no hemisfério norte, assim como um novo lançamento do GNOME!

2
00:00:06,074 --> 00:00:12,094
Neste lançamento, nós estamos trazendo para você novos temas padronizados e uma melhor experiência desenvolvimento.

3
00:00:13,060 --> 00:00:17,220
Vários aplicativos foram refeitos, como, por exemplo, o visualizador de imagens do GNOME.

4
00:00:17,220 --> 00:00:20,340
..que agora possui novos controles de interface de usuários.

5
00:00:26,075 --> 00:00:28,475
Arquivos também recebeu um pouco de amor..

6
00:00:28,494 --> 00:00:30,894
..com menus simplificados de janelas sobrepostas..

7
00:00:30,894 --> 00:00:32,994
..e um visualizador de arquivos melhorado.

8
00:00:36,280 --> 00:00:40,240
GNOME 3.16 também traz novos visuais para o GNOME Shell.

9
00:00:43,240 --> 00:00:45,280
Isso inclui o panorama de atividades..

10
00:00:45,740 --> 00:00:47,940
..o menu de aplicativos..

11
00:00:48,060 --> 00:00:50,200
..e o menu de sistema.

12
00:00:52,740 --> 00:00:57,700
GNOME Shell também fornece uma interface melhorada do sistema de notificação.

13
00:00:59,540 --> 00:01:03,880
Essas melhorias visam tornar notificações no GNOME mais visíveis..

14
00:01:03,920 --> 00:01:06,600
..mas também fácil de tirar de seu caminho.

15
00:01:09,380 --> 00:01:12,540
Este ciclo oferece três novos aplicativos GNOME.

16
00:01:12,800 --> 00:01:15,600
Um deles é uma versão em desenvolvimento da Agenda do GNOME..

17
00:01:15,800 --> 00:01:18,660
..um aplicativo que ajuda você a planejar suas atividades.

18
00:01:26,540 --> 00:01:29,000
Agenda do GNOME se integra com suas contas on-line..

19
00:01:29,260 --> 00:01:31,960
..de forma que você pode levar seu calendário com você.

20
00:01:32,820 --> 00:01:35,860
Este lançamento também oferece uma versão em desenvolvimento dos livros do GNOME..

21
00:01:35,980 --> 00:01:38,980
..um novo aplicativo para suas necessidades relacionadas a leitura de livros.

22
00:01:42,860 --> 00:01:45,420
Além do mais, este lançamento traz uma versão em desenvolvimento do Builder..

23
00:01:45,560 --> 00:01:47,407
..um novo aplicativo para desenvolvedores.

24
00:01:48,580 --> 00:01:52,360
Builder é um IDE que visa melhorar a experiência do desenvolvedor no GNOME.

25
00:01:55,360 --> 00:01:59,520
Graças aos mais de 500 doadores, o projeto atingiu um financiamento coletivo com sucesso.

26
00:02:00,160 --> 00:02:03,180
Se você deseja ajudar a melhorara a experiência do desenvolvedor também,..

27
00:02:03,300 --> 00:02:06,420
..visite a campanha de financiamento na descrição abaixo.

28
00:02:08,540 --> 00:02:12,240
GTK+ também visa melhorar a experiência de desenvolvimento neste ciclo,..

29
00:02:13,400 --> 00:02:15,780
..com várias melhorias no GtkInspector,..

30
00:02:15,780 --> 00:02:17,620
..suporte a OpenGl..

31
00:02:18,280 --> 00:02:19,634
..e um backend de mir.

32
00:02:20,720 --> 00:02:23,860
Finalmente, GNOME 3.16 vem com um glib melhorado,..

33
00:02:24,140 --> 00:02:27,200
..visando tornar a vida de desenvolvedores C muito mais fácil.

34
00:02:32,340 --> 00:02:36,180
O novo lançamento do GNOME está disponível para você como uma imagem Live para tentar agora.

35
00:02:37,420 --> 00:02:41,320
GNOME 3.16 será disponibilizado em várias distribuições muito em breve.

36
00:02:43,680 --> 00:02:45,660
GNOME é feito por pessoas para pessoas.

37
00:02:45,740 --> 00:02:49,260
Ajude-nos a tornar o GNOME melhor contribuindo desde hoje.
