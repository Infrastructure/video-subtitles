1
00:00:01,840 --> 00:00:06,074
Beköszöntött a tavasz az északi féltekén, így megérkezett a GNOME új kiadása!

2
00:00:06,074 --> 00:00:12,094
Ezzel a kiadással új, áramvonalas témázást és jobb fejlesztői élményt hozunk Önnek.

3
00:00:13,060 --> 00:00:17,220
Számos alkalmazás átalakításon esett át, például a GNOME képnézegetője.

4
00:00:17,220 --> 00:00:20,340
…amely most már új felhasználói felület vezérlőket nyújt.

5
00:00:26,075 --> 00:00:28,475
A Fájlok is kapott egy kis szeretetet…

6
00:00:28,494 --> 00:00:30,894
…az egyszerűsített felugró menükkel…

7
00:00:30,894 --> 00:00:32,994
…és egy továbbfejlesztett fájlnézettel.

8
00:00:36,280 --> 00:00:40,240
A GNOME 3.16 új megjelenítéseket is hoz a GNOME Shellhez.

9
00:00:43,240 --> 00:00:45,280
Ez magában foglalja a tevékenységek áttekintőt…

10
00:00:45,740 --> 00:00:47,940
…az alkalmazásmenüt…

11
00:00:48,060 --> 00:00:50,200
…és a rendszermenüt.

12
00:00:52,740 --> 00:00:57,700
A GNOME Shell egy továbbfejlesztett megjelenést is biztosít az értesítési rendszerhez.

13
00:00:59,540 --> 00:01:03,880
Ezen javítások célja, hogy az értesítéseket még láthatóbbá tegye a GNOME-ban…

14
00:01:03,920 --> 00:01:06,600
…de könnyen félrerakhatóak is legyenek.

15
00:01:09,380 --> 00:01:12,540
Ez a ciklus három új GNOME alkalmazást mutat be.

16
00:01:12,800 --> 00:01:15,600
Egyikük a GNOME Naptár előzetese…

17
00:01:15,800 --> 00:01:18,660
…egy alkalmazás, amely segít előre tervezni.

18
00:01:26,540 --> 00:01:29,000
A GNOME Naptár integrálja az online fiókokat…

19
00:01:29,260 --> 00:01:31,960
…így magával viheti a naptárát, bárhova is megy.

20
00:01:32,820 --> 00:01:35,860
Ez a kiadás a GNOME könyvek előzetesét is bemutatja…

21
00:01:35,980 --> 00:01:38,980
…egy új alkalmazást a könyvolvasási szükségleteihez.

22
00:01:42,860 --> 00:01:45,420
Ezenkívül ez a kiadás az Építő előzetesét is hozza…

23
00:01:45,560 --> 00:01:47,407
…egy új alkalmazást a fejlesztőknek.

24
00:01:48,580 --> 00:01:52,360
Az Építő egy IDE, amelynek célja, hogy fokozza a GNOME fejlesztői élményét.

25
00:01:55,360 --> 00:01:59,520
Köszönjük a több mint 500 támogatónak, hogy a projekt sikeresen finanszírozott.

26
00:02:00,160 --> 00:02:03,180
Ha segíteni szeretne a fejlesztői élmény növelésében is…

27
00:02:03,300 --> 00:02:06,420
…akkor látogassa meg a lenti leírásban lévő finanszírozási kampányt.

28
00:02:08,540 --> 00:02:12,240
A GTK+ célja szintén az, hogy növelje ezen ciklus fejlesztői élményét…

29
00:02:13,400 --> 00:02:15,780
…a GtkInspector számos továbbfejlesztésével…

30
00:02:15,780 --> 00:02:17,620
…OpenGL támogatással…

31
00:02:18,280 --> 00:02:19,634
…és egy mir háttérprogrammal.

32
00:02:20,720 --> 00:02:23,860
Végezetül a GNOME 3.16 egy továbbfejlesztett glib-et szállít…

33
00:02:24,140 --> 00:02:27,200
…amelynek célja a C-fejlesztők életének sokkal könnyebbé tétele.

34
00:02:32,340 --> 00:02:36,180
Az új GNOME kiadás elérhető Ön számára egy live lemezképben, hogy kipróbálhassa.

35
00:02:37,420 --> 00:02:41,320
A GNOME 3.16-ot számos disztribúció szállítani fogja a közeljövőben.

36
00:02:43,680 --> 00:02:45,660
A GNOME-ot emberek készítették embereknek.

37
00:02:45,740 --> 00:02:49,260
Segítsen nekünk a GNOME jobbá tételében, legyen még ma közreműködő.
