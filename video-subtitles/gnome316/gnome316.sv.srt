1
00:00:01,840 --> 00:00:06,074
Våren har kommit till norra halvklotet, och det har även en ny utgåva av GNOME!

2
00:00:06,074 --> 00:00:12,094
Med denna utgåva ger vi dig ett nytt strömlinjeformat tema och en bättre utvecklarupplevelse.

3
00:00:13,060 --> 00:00:17,220
Flera program har omstrukturerats, till exempel GNOME:s bildvisare.

4
00:00:17,220 --> 00:00:20,340
…som nu har nya kontroller i användargränssnittet.

5
00:00:26,075 --> 00:00:28,475
Filer har också fått lite kärlek…

6
00:00:28,494 --> 00:00:30,894
…med förenklade kontextmenyer…

7
00:00:30,894 --> 00:00:32,994
…och en förbättrad filvy.

8
00:00:36,280 --> 00:00:40,240
GNOME 3.16 ger också ett nytt utseende till GNOME Shell.

9
00:00:43,240 --> 00:00:45,280
Detta inkluderar aktivitetsöversiktsvyn…

10
00:00:45,740 --> 00:00:47,940
…programmenyn…

11
00:00:48,060 --> 00:00:50,200
…och systemmenyn.

12
00:00:52,740 --> 00:00:57,700
GNOME Shell tillhandahåller också en förbättrad design för aviseringssystemet.

13
00:00:59,540 --> 00:01:03,880
Dessa förbättringar siktar mot att göra aviseringar i GNOME lättare att upptäcka…

14
00:01:03,920 --> 00:01:06,600
…men ändå lätta att få att sluta vara i vägen.

15
00:01:09,380 --> 00:01:12,540
Denna cykel presenterar tre nya GNOME-program.

16
00:01:12,800 --> 00:01:15,600
Ett av dem är en förhandsversion av GNOME Kalender…

17
00:01:15,800 --> 00:01:18,660
…ett program som låter dig planera framåt.

18
00:01:26,540 --> 00:01:29,000
GNOME Kalender integrerar med dina nätkonton…

19
00:01:29,260 --> 00:01:31,960
…så du kan ta din kalender med dig på resan.

20
00:01:32,820 --> 00:01:35,860
Denna utgåva innehåller också en förhandsversion av GNOME böcker…

21
00:01:35,980 --> 00:01:38,980
…ett nytt program för dina behov av att läsa böcker.

22
00:01:42,860 --> 00:01:45,420
Vidare erbjuder denna utgåva en förhandsvisning av Builder…

23
00:01:45,560 --> 00:01:47,407
…ett nytt program för utvecklare.

24
00:01:48,580 --> 00:01:52,360
Builder är en utvecklingsmiljö (IDE) vars mål är att förbättra utvecklarupplevelsen i GNOME.

25
00:01:55,360 --> 00:01:59,520
Projektet finansierades tack vare över 500 bidragsgivare.

26
00:02:00,160 --> 00:02:03,180
Om du också vill hjälpa till att förbättra utvecklarupplevelsen,…

27
00:02:03,300 --> 00:02:06,420
…besök gräsrotsfinansieringskampanjen i beskrivningen nedan.

28
00:02:08,540 --> 00:02:12,240
GTK+ siktar också mot att förbättra utvecklarupplevelsen under denna cykel,…

29
00:02:13,400 --> 00:02:15,780
..med många förbättringar till GtkInspector,…

30
00:02:15,780 --> 00:02:17,620
…stöd för OpenGL…

31
00:02:18,280 --> 00:02:19,634
…och en mir-bakände.

32
00:02:20,720 --> 00:02:23,860
Slutligen kommer GNOME 3.16 med ett förbättrat glib,…

33
00:02:24,140 --> 00:02:27,200
…med målet att göra livet mycket enklare för C-utvecklare.

34
00:02:32,340 --> 00:02:36,180
Den nya GNOME-utgåvan är tillgänglig för dig som en live-avbild att testa nu.

35
00:02:37,420 --> 00:02:41,320
GNOME 3.16 kommer att levereras av många distributioner i en nära framtid.

36
00:02:43,680 --> 00:02:45,660
GNOME görs av människor för människor.

37
00:02:45,740 --> 00:02:49,260
Hjälp oss göra GNOME bättre genom att engagera dig idag.
