1
00:00:01,840 --> 00:00:06,074
Na severní polokouli přichází jaro a tak je tu nové vydání GNOME!

2
00:00:06,074 --> 00:00:12,094
Toto vydání přináší nově odlehčené motivy a lepší přívětivost pro vývojáře.

3
00:00:13,060 --> 00:00:17,220
Několik aplikací bylo přepracováno, například prohlížeč obrázků pro GNOME…

4
00:00:17,220 --> 00:00:20,340
…který nyní obsahuje nové ovládací prvky uživatelského rozhraní.

5
00:00:26,075 --> 00:00:28,475
Péče byla věnována i aplikaci Soubory…

6
00:00:28,494 --> 00:00:30,894
…ve zjednodušení nabídek s robalovacími dialogovými okny…

7
00:00:30,894 --> 00:00:32,994
…a zdokonaleném zobrazení souborů.

8
00:00:36,280 --> 00:00:40,240
GNOME 3.16 rovněž přináší nový vizuální styl do GNOME Shell.

9
00:00:43,240 --> 00:00:45,280
Týká se to přehledu činností…

10
00:00:45,740 --> 00:00:47,940
…nabídky aplikací…

11
00:00:48,060 --> 00:00:50,200
…a systémové nabídky.

12
00:00:52,740 --> 00:00:57,700
GNOME Shell také poskytuje vylepšený vzhled systému upozorňování.

13
00:00:59,540 --> 00:01:03,880
Cílem těchto vylepšení je, abyste si upozornění v GNOME spíše všimli…

14
00:01:03,920 --> 00:01:06,600
…ale na druhou stranu vám méně překážela.

15
00:01:09,380 --> 00:01:12,540
V tomto cyklu přicházejí tři nové aplikace GNOME.

16
00:01:12,800 --> 00:01:15,600
Jednou z nich je ukázková verze Kalendáře GNOME…

17
00:01:15,800 --> 00:01:18,660
…aplikace, která vám pomáhá udržet si přehled o svých plánech.

18
00:01:26,540 --> 00:01:29,000
Kalendář GNOME je provázán s vašimi účty on-line…

19
00:01:29,260 --> 00:01:31,960
…takže svůj kalendář máte po ruce i na cestách.

20
00:01:32,820 --> 00:01:35,860
Toto vydání uvádí také ukázkovou verzi Knih GNOME…

21
00:01:35,980 --> 00:01:38,980
…nové aplikace sloužící ke čtení e-knih.

22
00:01:42,860 --> 00:01:45,420
A nakonec toto vydání přináší v ukázkové verzi Builder…

23
00:01:45,560 --> 00:01:47,407
…novou aplikaci pro vývojáře.

24
00:01:48,580 --> 00:01:52,360
Builder je IDE snažící se zlepšit přívětivost pro vývojáře v GNOME.

25
00:01:55,360 --> 00:01:59,520
Díky více než 500 přispěvatelů měl projekt úspěšnou finanční kampaň.

26
00:02:00,160 --> 00:02:03,180
Jestli chcete i vy pomoci vylepšit přívětivost pro vývojáře,…

27
00:02:03,300 --> 00:02:06,420
…použíjte crowdfunding popsaný níže.

28
00:02:08,540 --> 00:02:12,240
I GTK+ se v tomto cyklu zaměřilo na lepší přívětivost pro vývojáře…

29
00:02:13,400 --> 00:02:15,780
…pomocí mnoha vylepšení v GtkInspector, …

30
00:02:15,780 --> 00:02:17,620
…v podpoře pro OpenGL…

31
00:02:18,280 --> 00:02:19,634
…a v serveru mir.

32
00:02:20,720 --> 00:02:23,860
K tomu GNOME 3.16 obsahuje knihovnu glib vylepšenou…

33
00:02:24,140 --> 00:02:27,200
…z pohledu snadnějšího používání vývojáři C.

34
00:02:32,340 --> 00:02:36,180
Vydání GNOME je vám k dispozici na vyzkoušení v podobě živého obrazu disku.

35
00:02:37,420 --> 00:02:41,320
V dohledné době se pak GNOME 3.16 dostane do řady distribucí.

36
00:02:43,680 --> 00:02:45,660
GNOME vytváří lidé pro lidi.

37
00:02:45,740 --> 00:02:49,260
Pomozte nám udělat GNOME ještě lepší tím, že se zapojíte.
