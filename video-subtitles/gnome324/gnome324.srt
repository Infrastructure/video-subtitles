1
00:00:04,320 --> 00:00:07,280
GNOME is a free desktop for your computer.

2
00:00:07,560 --> 00:00:12,340
It's a large project creating a modern and
elegant desktop experience.

3
00:00:12,500 --> 00:00:15,420
Behind the project is a community of contributors,..

4
00:00:15,580 --> 00:00:19,080
..who have worked together to bring you GNOME 3.24.

5
00:00:19,660 --> 00:00:21,940
The release includes quite a lot of changes..

6
00:00:21,940 --> 00:00:25,640
..to polish and fortify GNOME's applications and libraries.

7
00:00:25,640 --> 00:00:29,680
Within the past year, GNOME's control center
has been a focus.

8
00:00:30,760 --> 00:00:32,260
The User Accounts panel,..

9
00:00:33,120 --> 00:00:34,520
..Online Accounts panel,..

10
00:00:35,220 --> 00:00:38,180
..and the Printer panel have been modernized.

11
00:00:40,080 --> 00:00:44,800
The display panel supports automatically shifting
the screen light according to time of day.

12
00:00:45,420 --> 00:00:47,880
This create less strain on your eyes at night.

13
00:00:49,700 --> 00:00:53,280
If you own a drawing tablet, you can now use
it under Wayland.

14
00:00:53,840 --> 00:00:56,280
The Wacom tablet panel also includes..

15
00:00:56,280 --> 00:00:59,240
.. a small drawing area you can use to test your settings with.

16
00:01:01,120 --> 00:01:03,700
The GNOME community values your privacy.

17
00:01:04,020 --> 00:01:05,860
That's why, by default,..

18
00:01:05,860 --> 00:01:09,660
..the new release of GNOME Web protects you from being tracked when browsing the Internet.

19
00:01:10,420 --> 00:01:13,560
Web also comes with a new popover menu for
bookmarks,..

20
00:01:13,560 --> 00:01:17,280
..enabling you to easily tag and organize your favorite websites.

21
00:01:18,400 --> 00:01:22,060
Many other applications have also improved
this cycle including..

22
00:01:23,020 --> 00:01:27,120
Calendar, which now has a weekly view to help
you manage your schedule.

23
00:01:29,000 --> 00:01:33,540
Polari, with new contextual popovers and notification
functionality.

24
00:01:36,760 --> 00:01:39,420
And Recipes, which is a new application..,

25
00:01:39,540 --> 00:01:42,760
..containing recipes made by many members of the GNOME community.

26
00:01:47,700 --> 00:01:50,560
Developers can look forward to an improved
GNOME Builder,..

27
00:01:50,560 --> 00:01:54,560
..with solid support for building containerized applications with flatpak.

28
00:01:55,320 --> 00:01:57,760
Builder also supports multiple build systems,..

29
00:01:57,760 --> 00:02:01,280
..such as CMake, Autotools, Cargo and Meson.

30
00:02:03,080 --> 00:02:05,020
Many of these features have come to be,..

31
00:02:05,020 --> 00:02:07,060
..thanks to heroes around the globe,..

32
00:02:07,060 --> 00:02:10,280
..who have taken the initiative to organize events for the
GNOME community.

33
00:02:11,040 --> 00:02:16,520
During the work on GNOME 3.24 a number of
GNOME events took place such as:

34
00:02:17,300 --> 00:02:19,420
..The Libre Application Summit in Portland

35
00:02:20,240 --> 00:02:24,300
..The GNOME Bug Squash Month initiative with 6 participating local groups.

36
00:02:25,080 --> 00:02:27,120
..The Core Apps Hackfest in Berlin.

37
00:02:28,440 --> 00:02:30,000
..and many other great events.

38
00:02:32,360 --> 00:02:35,380
GNOME could not exist without its supporting community.

39
00:02:35,540 --> 00:02:37,980
The project consists of many teams of contributors,..

40
00:02:37,980 --> 00:02:41,220
..who volunteer their time to help advance the free desktop.

41
00:02:41,920 --> 00:02:46,680
Are you interested in helping with engagement,
translation, documentation or coding?

42
00:02:47,300 --> 00:02:50,040
We have guides and friendly people to help
you get started.

43
00:02:50,500 --> 00:02:54,400
With your help, free software and GNOME can
continue to grow.

