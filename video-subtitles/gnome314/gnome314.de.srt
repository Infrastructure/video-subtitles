1
00:00:04,000 --> 00:00:06,500
Danke für die Mitarbeit der vielen Beteiligten …

2
00:00:06,800 --> 00:00:08,000
… und danke an die Freunde von GNOME

3
00:00:08,200 --> 00:00:11,000
… wir sind stolz, GNOME 3.14 bekannt geben zu dürfen.

4
00:00:11,800 --> 00:00:15,200
Mit dieser Version liefert GNOME eine verbesserte Arbeitsumgebung …

5
00:00:15,200 --> 00:00:16,900
… für seine Benutzer und Entwickler.

6
00:00:18,000 --> 00:00:20,700
Die optischen Details in GNOME wurden überarbeitet …

7
00:00:20,800 --> 00:00:23,600
… und neue Animationen implementiert …

8
00:00:23,600 --> 00:00:25,200
… damit Ihr Nutzererlebnis verbessert wird.

9
00:00:26,300 --> 00:00:29,500
Diese Version bringt ebenfalls eine bessere Unterstützung für Multitouch.

10
00:00:30,880 --> 00:00:32,900
Sie können nun wischen und die Größe ändern …

11
00:00:32,900 --> 00:00:34,200
… in vielen GNOME-Anwendungen.

12
00:00:35,680 --> 00:00:38,200
Die GNOME-Shell kennt ebenfalls neue Gesten.

13
00:00:38,720 --> 00:00:41,700
Zum Beispiel beim Wechseln der Arbeitsfläche …

14
00:00:41,700 --> 00:00:44,200
… und beim Öffnen der Aktivitäten-Übersicht.

15
00:00:45,200 --> 00:00:47,700
Mit Hilfe der GNOME-Gemeinschaft …

16
00:00:47,700 --> 00:00:50,100
… und der diesjährigen Praktikanten …

17
00:00:50,100 --> 00:00:52,600
… konnten viele GNOME-Anwendungen aktualisiert werden.

18
00:00:54,000 --> 00:00:56,440
»Software« unterstützt nun Erweiterungen für Anwendungen.

19
00:00:57,000 --> 00:01:00,000
»Karten« verfügt über eine Navigation.

20
00:01:01,000 --> 00:01:03,420
»Fotos« importiert jetzt Ihre Bilder von Google.

21
00:01:04,000 --> 00:01:07,000
Und die Oberfläche von »Wetter« wurde neu gestaltet.

22
00:01:08,800 --> 00:01:13,900
Darüber hinaus bieten »Uhren« und »Rechner« neue Suchdienste …

23
00:01:13,900 --> 00:01:15,800
… damit Sie schauen können, wie spät es ist …

24
00:01:15,800 --> 00:01:17,800
… und direkt im Suchfeld rechnen können.

25
00:01:18,200 --> 00:01:21,400
Sudoku und Minen wurden neu gestaltet …

26
00:01:21,400 --> 00:01:22,960
… für ein besseres Spielerlebnis.

27
00:01:23,360 --> 00:01:25,200
Und ein neues Spiel, Hitori …

28
00:01:25,220 --> 00:01:27,800
… ist nun ein Teil der GNOME-Spiele.

29
00:01:30,200 --> 00:01:33,000
Mit GNOME 3.14 können sich Entwickler …

30
00:01:33,000 --> 00:01:35,000
… über eine bessere Entwicklungsumgebung freuen.

31
00:01:35,700 --> 00:01:37,740
GTK arbeitet fortwährend …

32
00:01:37,740 --> 00:01:39,500
… an einem verbesserten Erlebnis für Entwickler.

33
00:01:40,860 --> 00:01:43,000
Diese Version wird mit dem GTK-Inspektor ausgeliefert …

34
00:01:43,240 --> 00:01:46,940
… einem interaktiven Fehlediagnosewerkzeug, der den Arbeitsablauf bei der Fehlersuche verbessern soll.

35
00:01:50,560 --> 00:01:53,040
GTK hat nun eine erweiterte Dokumentation …

36
00:01:53,280 --> 00:01:56,520
… einschließlich einem neuen Regelwerk für die Gestaltung der Benutzeroberfläche.

37
00:01:59,560 --> 00:02:01,800
Neben den Neuigkeiten für GTK …

38
00:02:01,800 --> 00:02:03,400
… bringt diese Version mehr Stabilität …

39
00:02:03,400 --> 00:02:05,300
… für den GNOME-Kompositor unter Wayland.

40
00:02:06,300 --> 00:02:08,640
Darüber hinaus hat glib neue Funktionen …

41
00:02:09,040 --> 00:02:10,860
… wie z.B. volle Unterstützung für Unicode 7 …

42
00:02:10,960 --> 00:02:13,800
… und einer Unterstützung der neuen Mimeapps-Spezifikationen.

43
00:02:15,580 --> 00:02:18,800
Viele Distributionen werden demnächst GNOME 3.14 ausliefern.

44
00:02:20,400 --> 00:02:21,780
Wenn Sie es nicht erwarten können …

45
00:02:22,020 --> 00:02:23,800
Probieren Sie unser Demo-Abbild aus der Beschreibung.

46
00:02:24,800 --> 00:02:27,800
Oder helfen Sie mit und machen Sie die nächste GNOME-Version noch besser.

47
00:02:29,400 --> 00:02:31,900
Wir freuen uns, Ihnen beim Einstieg zu helfen.
