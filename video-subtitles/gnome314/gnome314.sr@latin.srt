1
00:00:04,000 --> 00:00:06,500
Zahvaljujući napornom radu naših saradnika..

2
00:00:06,800 --> 00:00:08,000
..i zahvaljujući prijateljima Gnoma

3
00:00:08,200 --> 00:00:11,000
..ponosni smo da objavimo Gnom 3.14.

4
00:00:11,800 --> 00:00:15,200
Sa ovim izdanjem Gnom donosi poboljšanu radnu površ..

5
00:00:15,200 --> 00:00:16,900
..za svoje korisnike i programere.

6
00:00:18,000 --> 00:00:20,700
Vidljive pojedinosti u Gnomu su izmenjene..

7
00:00:20,800 --> 00:00:23,600
..i primenjene su nove animacije zarad..

8
00:00:23,600 --> 00:00:25,200
..poboljšanja vašeg korisničkog iskustva.

9
00:00:26,300 --> 00:00:29,500
Ovo izdanje donosi i bolju podršku za multidodir.

10
00:00:30,880 --> 00:00:32,900
Sada možete da koristite prevlačenje..

11
00:00:32,900 --> 00:00:34,200
..i uveličavanje u mnogim Gnomovim programima.

12
00:00:35,680 --> 00:00:38,200
Takođe i Gnomova školjka ima nove pokrete.

13
00:00:38,720 --> 00:00:41,700
Na primer, prilikom promene radnih prostora..

14
00:00:41,700 --> 00:00:44,200
..i otvaranja pregleda aktivnosti.

15
00:00:45,200 --> 00:00:47,700
Uz pomoć Gnomove zajednice i od..

16
00:00:47,700 --> 00:00:50,100
..od ovogodišnjih učesnika studentske prakse..

17
00:00:50,100 --> 00:00:52,600
..mnogi Gnomovi programi su dobili osvežavanja.

18
00:00:54,000 --> 00:00:56,440
Programi sada podržava dodatke programa.

19
00:00:57,000 --> 00:01:00,000
Karte je dobio usmeravanje kretanja.

20
00:01:01,000 --> 00:01:03,420
Fotografije može da uveze vaše fotografije sa Gugla.

21
00:01:04,000 --> 00:01:07,000
A sučelje Vremena je ponovo odrađeno.

22
00:01:08,800 --> 00:01:13,900
Pored toga, satovi i kalkulator imaju nove dobavljače pretrage..

23
00:01:13,900 --> 00:01:15,800
..tako da možete da proverite vreme..

24
00:01:15,800 --> 00:01:17,800
..i da izračunate uživo u polju pretrage.

25
00:01:18,200 --> 00:01:21,400
Sidoku i mine su ponovo odrađeni..

26
00:01:21,400 --> 00:01:22,960
..zarad boljeg iskustva igranja igre.

27
00:01:23,360 --> 00:01:25,200
A nova igra, pod nazivom Hitori..

28
00:01:25,220 --> 00:01:27,800
..je sada deo Gnomovih igara.

29
00:01:30,200 --> 00:01:33,000
Sa Gnomom 3.14, programeri se mogu nadati..

30
00:01:33,000 --> 00:01:35,000
..boljem razvojnom okruženju.

31
00:01:35,700 --> 00:01:37,740
Gtk je započeo rad na.. 

32
00:01:37,740 --> 00:01:39,500
..poboljšanju programerskog iskustva.

33
00:01:40,860 --> 00:01:43,000
Ovo izdanje dolazi sa Gtk Nadzornikom..

34
00:01:43,240 --> 00:01:46,940
međudejstvenim pročišćavačem koji ima za cilj da poboljša radni tok pročišćavanja.

35
00:01:50,560 --> 00:01:53,040
Gtk dolazi i sa više dokumentacije..

36
00:01:53,280 --> 00:01:56,520
..uključujući potpuno novi skup smernica korisničkog sučelja.

37
00:01:59,560 --> 00:02:01,800
Pored novosti o Gtk-u..

38
00:02:01,800 --> 00:02:03,400
..ovo izdanje donosi veću stabilnost..

39
00:02:03,400 --> 00:02:05,300
..Gnomovom sastavljaču u Vajlandu.

40
00:02:06,300 --> 00:02:08,640
Pored toga, glib je dobila nove funkcije..

41
00:02:09,040 --> 00:02:10,860
..kao što je saglasnost sa Unikodom 7..

42
00:02:10,960 --> 00:02:13,800
..i podrška za novu odredbu Mimeaps-a.

43
00:02:15,580 --> 00:02:18,800
Mnoge distribucije će u budućnosti isporučivati Gnom 3.14.

44
00:02:20,400 --> 00:02:21,780
Ako ne možete da sačekate..

45
00:02:22,020 --> 00:02:23,800
Isprobajte našu probnu sliku u opisu.

46
00:02:24,800 --> 00:02:27,800
Ili nam se pridružite u stvaranju još boljeg sledećeg izdanja Gnoma. 

47
00:02:29,400 --> 00:02:31,900
Željno vas očekujemo.
