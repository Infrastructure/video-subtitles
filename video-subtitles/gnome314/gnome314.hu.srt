1
00:00:04,000 --> 00:00:06,500
Köszönjük az együttműködők kemény munkáját…

2
00:00:06,800 --> 00:00:08,000
…és köszönjük a GNOME barátainak

3
00:00:08,200 --> 00:00:11,000
…örömmel jelentjük be a GNOME 3.14-et.

4
00:00:11,800 --> 00:00:15,200
Ezzel a kiadással a GNOME egy továbbfejlesztett asztalt hoz…

5
00:00:15,200 --> 00:00:16,900
…a felhasználóknak és a fejlesztőknek.

6
00:00:18,000 --> 00:00:20,700
A GNOME látható részleteit felülvizsgálták…

7
00:00:20,800 --> 00:00:23,600
…és új animációkat valósítottak meg…

8
00:00:23,600 --> 00:00:25,200
…a felhasználói élmény fokozása érdekében.

9
00:00:26,300 --> 00:00:29,500
Ez a kiadás jobb támogatást nyújt a többérintéses eszközökhöz.

10
00:00:30,880 --> 00:00:32,900
Most már használhat pöccintést és nagyítást…

11
00:00:32,900 --> 00:00:34,200
…a GNOME appok nagy tömegében.

12
00:00:35,680 --> 00:00:38,200
A GNOME Shell is új gesztusokat kapott.

13
00:00:38,720 --> 00:00:41,700
Például munkaterületek közötti váltáskor…

14
00:00:41,700 --> 00:00:44,200
…és a tevékenységek áttekintőjének megnyitásakor.

15
00:00:45,200 --> 00:00:47,700
A GNOME közösségtől érkező segítséggel…

16
00:00:47,700 --> 00:00:50,100
…és az idei év gyakornoki résztvevőitől…

17
00:00:50,100 --> 00:00:52,600
…rengeteg GNOME app kapott frissítést.

18
00:00:54,000 --> 00:00:56,440
A Szoftverek most már támogatja az app-bővítményeket.

19
00:00:57,000 --> 00:01:00,000
A Térkép navigációs útvonaltervezést szerzett.

20
00:01:01,000 --> 00:01:03,420
A Fényképek importálja a fényképeit a Google-től.

21
00:01:04,000 --> 00:01:07,000
És az Időjárás felületét újratervezték.

22
00:01:08,800 --> 00:01:13,900
Továbbá az óra és a számológép új keresőszolgáltatót kapott…

23
00:01:13,900 --> 00:01:15,800
…így ellenőrizheti az időt…

24
00:01:15,800 --> 00:01:17,800
…és élőben számolhat a keresőmezőben.

25
00:01:18,200 --> 00:01:21,400
A Sudoku és az aknakereső újra lett tervezve…

26
00:01:21,400 --> 00:01:22,960
…a jobb játékmeneti élmény érdekében.

27
00:01:23,360 --> 00:01:25,200
És egy új játék, amelynek neve Hitori…

28
00:01:25,220 --> 00:01:27,800
…most már a GNOME játékok része.

29
00:01:30,200 --> 00:01:33,000
A GNOME 3.14-gyel a fejlesztők előre örülhetnek…

30
00:01:33,000 --> 00:01:35,000
…egy jobb fejlesztői környezetnek.

31
00:01:35,700 --> 00:01:37,740
A GTK megkezdte a folyamatos erőfeszítést…

32
00:01:37,740 --> 00:01:39,500
…a fejlesztői élmény fokozásához.

33
00:01:40,860 --> 00:01:43,000
Ez a kiadás a GTK felügyelővel érkezik…

34
00:01:43,240 --> 00:01:46,940
egy interaktív hibakeresővel, amelynek célja a hibakereső munkafolyamat javítása.

35
00:01:50,560 --> 00:01:53,040
A GTK több dokumentációval is érkezik…

36
00:01:53,280 --> 00:01:56,520
…beleértve a teljesen új emberi felületirányelvek halmazát.

37
00:01:59,560 --> 00:02:01,800
Eltekintve a GTK híreitől…

38
00:02:01,800 --> 00:02:03,400
…ez a kiadás jobb stabilitást hoz…

39
00:02:03,400 --> 00:02:05,300
…a GNOME betűszedőhöz a Waylandben.

40
00:02:06,300 --> 00:02:08,640
Továbbá a glib új szolgáltatásokat szerzett…

41
00:02:09,040 --> 00:02:10,860
…mint például a Unicode 7 megfelelőség…

42
00:02:10,960 --> 00:02:13,800
…és az új Mimeapps specifikáció támogatása.

43
00:02:15,580 --> 00:02:18,800
Sok disztribúció hamarosan szállítani fogja a GNOME 3.14-et.

44
00:02:20,400 --> 00:02:21,780
Ha nem tud addig várni…

45
00:02:22,020 --> 00:02:23,800
Próbálja ki a leírásban lévő bemutató lemezképünket.

46
00:02:24,800 --> 00:02:27,800
Vagy csatlakozzon hozzánk a következő GNOME kiadás még jobbá tételéhez.

47
00:02:29,400 --> 00:02:31,900
Bízunk benne, hogy belevág.
