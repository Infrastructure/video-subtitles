1
00:00:04,000 --> 00:00:06,500
Dzięki pracy naszych programistów

2
00:00:06,800 --> 00:00:08,000
a także przyjaciół GNOME

3
00:00:08,200 --> 00:00:11,000
możemy ogłosić wydanie GNOME 3.14.

4
00:00:11,800 --> 00:00:15,200
W tym wydaniu ulepszono całe środowisko

5
00:00:15,200 --> 00:00:16,900
dla użytkowników i programistów.

6
00:00:18,000 --> 00:00:20,700
Szczegóły wyglądu GNOME zostały poprawione

7
00:00:20,800 --> 00:00:23,600
oraz zaimplementowano nowe animacje

8
00:00:23,600 --> 00:00:25,200
co ułatwia jego używanie.

9
00:00:26,300 --> 00:00:29,500
To wydanie lepiej obsługuje gesty wielodotykowe.

10
00:00:30,880 --> 00:00:32,900
Można teraz przewijać i przybliżać

11
00:00:32,900 --> 00:00:34,200
w wielu programach GNOME.

12
00:00:35,680 --> 00:00:38,200
Powłoka GNOME także obsługuje nowe gesty

13
00:00:38,720 --> 00:00:41,700
na przykład do przełączania przestrzeni roboczych

14
00:00:41,700 --> 00:00:44,200
i otwierania ekranu podglądu.

15
00:00:45,200 --> 00:00:47,700
Z pomocą społeczności GNOME

16
00:00:47,700 --> 00:00:50,100
i tegorocznych stażystów

17
00:00:50,100 --> 00:00:52,600
wiele programów GNOME zostało zaktualizowanych.

18
00:00:54,000 --> 00:00:56,440
Menedżer oprogramowania obsługuje dodatki.

19
00:00:57,000 --> 00:01:00,000
Mapy umożliwiają znajdowanie tras.

20
00:01:01,000 --> 00:01:03,420
Menedżer zdjęć może importować zdjęcia z Google.

21
00:01:04,000 --> 00:01:07,000
Interfejs Pogody został przeprojektowany.

22
00:01:08,800 --> 00:01:13,900
Co więcej, Zegar i Kalkulator umożliwiają teraz

23
00:01:13,900 --> 00:01:15,800
sprawdzanie czasu

24
00:01:15,800 --> 00:01:17,800
i liczenie w polu wyszukiwania.

25
00:01:18,200 --> 00:01:21,400
Gry Sudoku i Miny zostały przeprojektowane

26
00:01:21,400 --> 00:01:22,960
aby lepiej się w nie grało.

27
00:01:23,360 --> 00:01:25,200
Nowa gra o nazwie Hitori

28
00:01:25,220 --> 00:01:27,800
jest teraz częścią gier GNOME.

29
00:01:30,200 --> 00:01:33,000
W GNOME 3.14 programiści mogą spodziewać się

30
00:01:33,000 --> 00:01:35,000
lepszego środowiska programistycznego.

31
00:01:35,700 --> 00:01:37,740
Rozpoczęto pracę nad ulepszeniem

32
00:01:37,740 --> 00:01:39,500
procesu programowania za pomocą GTK+.

33
00:01:40,860 --> 00:01:43,000
To wydanie zawiera Inspektora GTK+

34
00:01:43,240 --> 00:01:46,940
ułatwiającego interaktywne debugowanie programów.

35
00:01:50,560 --> 00:01:53,040
GTK+ ma teraz więcej dokumentacji

36
00:01:53,280 --> 00:01:56,520
w tym zupełnie nowe wytyczne projektowania interfejsu.

37
00:01:59,560 --> 00:02:01,800
Oprócz zmian w GTK+

38
00:02:01,800 --> 00:02:03,400
to wydanie posiada także zwiększoną

39
00:02:03,400 --> 00:02:05,300
stabilność GNOME w technologii Wayland.

40
00:02:06,300 --> 00:02:08,640
Co więcej, GLib uzyskało nowe funkcje

41
00:02:09,040 --> 00:02:10,860
w tym zgodność z Unicode 7

42
00:02:10,960 --> 00:02:13,800
i obsługę nowej specyfikacji MIME.

43
00:02:15,580 --> 00:02:18,800
W przyszłości wiele dystrybucji będzie zawierało GNOME 3.14.

44
00:02:20,400 --> 00:02:21,780
Jeśli nie chcesz czekać

45
00:02:22,020 --> 00:02:23,800
w opisie znajdziesz obraz demonstracyjny.

46
00:02:24,800 --> 00:02:27,800
Dołącz do nas, aby następne wydanie GNOME było jeszcze lepsze.

47
00:02:29,400 --> 00:02:31,900
Czekamy na ciebie.
