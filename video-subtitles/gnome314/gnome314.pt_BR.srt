1
00:00:04,000 --> 00:00:06,500
Graças ao trabalho árduo de nossos contribuidores..

2
00:00:06,800 --> 00:00:08,000
..e graças aos amigos do GNOME

3
00:00:08,200 --> 00:00:11,000
..estamos orgulhosos em anunciar GNOME 3.14

4
00:00:11,800 --> 00:00:15,200
Com este lançamento, GNOME traz um ambiente melhorado..

5
00:00:15,200 --> 00:00:16,900
..para seus usuários e desenvolvedores.

6
00:00:18,000 --> 00:00:20,700
Os detalhes visuais no GNOME foram revisados...

7
00:00:20,800 --> 00:00:23,600
..e novas animações foram implementadas..

8
00:00:23,600 --> 00:00:25,200
..para melhorar sua experiência como usuário.

9
00:00:26,300 --> 00:00:29,500
Esse lançamento também traz melhor suporte a multitoque.

10
00:00:30,880 --> 00:00:32,900
Você agora pode deslizar e ampliar..

11
00:00:32,900 --> 00:00:34,200
.. em um monte de aplicativos do GNOME.

12
00:00:35,680 --> 00:00:38,200
GNOME Shell também possui novos gestos.

13
00:00:38,720 --> 00:00:41,700
Por exemplo, ao alternar entre espaços de trabalhos..

14
00:00:41,700 --> 00:00:44,200
..e abrir o panorama de atividades.

15
00:00:45,200 --> 00:00:47,700
Com ajuda da comunidade do GNOME..

16
00:00:47,700 --> 00:00:50,100
..e dos participantes deste ano..

17
00:00:50,100 --> 00:00:52,600
..um monte de aplicativos do GNOME receberam atualizações.

18
00:00:54,000 --> 00:00:56,440
Programas agora suporta complementos de aplicativos.

19
00:00:57,000 --> 00:01:00,000
Mapas ganhou rota de navegação.

20
00:01:01,000 --> 00:01:03,420
Fotos pode umportar suas fotos do Google.

21
00:01:04,000 --> 00:01:07,000
E a interface do Meteorologia foi redesenhada.

22
00:01:08,800 --> 00:01:13,900
Além disso, relógios e calculadora possuem novos provedores de pesquisa..

23
00:01:13,900 --> 00:01:15,800
..de forma que você possa verificar o tempo..

24
00:01:15,800 --> 00:01:17,800
..e calcular em tempo real no campo de pesquisa.

25
00:01:18,200 --> 00:01:21,400
Sudoku e Minas foram redesenhados..

26
00:01:21,400 --> 00:01:22,960
..pra uma melhor experiência de jogabilidade.

27
00:01:23,360 --> 00:01:25,200
 E um novo jogo, chamado Hitori,..

28
00:01:25,220 --> 00:01:27,800
..agora faz parte dos jogos do GNOME.

29
00:01:30,200 --> 00:01:33,000
Com GNOME 3.14, desenvolvedores podem esperar por..

30
00:01:33,000 --> 00:01:35,000
.. um ambiente de desenvolvimento melhor.

31
00:01:35,700 --> 00:01:37,740
GTK começou um esforço contínuo.. 

32
00:01:37,740 --> 00:01:39,500
..para melhorar a experiência do desenvolvedor.

33
00:01:40,860 --> 00:01:43,000
Este lançamento vem com Inspetor GTK..

34
00:01:43,240 --> 00:01:46,940
um depurador interativo destinado a melhorar o fluxo de seu trabalho de depuração.

35
00:01:50,560 --> 00:01:53,040
GTK também vem com mais documentação..

36
00:01:53,280 --> 00:01:56,520
..incluindo uma nova coleçã de diretrizes de interface humana.

37
00:01:59,560 --> 00:02:01,800
Além das notíficas para GTK..

38
00:02:01,800 --> 00:02:03,400
..este lançamento traz mais estabilidade..

39
00:02:03,400 --> 00:02:05,300
..para o compositor GNOME no Wayland.

40
00:02:06,300 --> 00:02:08,640
Além disso, glib ganhou novos recursos..

41
00:02:09,040 --> 00:02:10,860
..como conformidade com Unicode 7..

42
00:02:10,960 --> 00:02:13,800
..e suporte para nova especificação de Mimeapps.

43
00:02:15,580 --> 00:02:18,800
Muitras distribuições vão fornecer GNOME 3.14 no futuro.

44
00:02:20,400 --> 00:02:21,780
Se você não puder esperar..

45
00:02:22,020 --> 00:02:23,800
tente nossa imagem de demonstração na descrição.

46
00:02:24,800 --> 00:02:27,800
Ou se junte a nós para do próximo lançamento do GNOME ainda melhor.

47
00:02:29,400 --> 00:02:31,900
Nós estamos ansiosos para seus primeiros passos.
