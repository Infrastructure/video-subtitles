1
00:00:04,000 --> 00:00:06,500
ਸਾਡੇ ਯੋਗਦਾਨੀਆਂ ਦੀ ਸਖ਼ਤ ਮੇਹਨਤ ਦਾ ਧੰਨਵਾਦ..

2
00:00:06,800 --> 00:00:08,000
..ਅਤੇ ਗਨੋਮ ਦੇ ਦੋਸਤਾਂ ਦਾ ਧੰਨਵਾਦ

3
00:00:08,200 --> 00:00:11,000
..ਅਸੀਂ ਬੜੇ ਮਾਣ ਨਾਲ ਗਨੋਮ ੩.੧੪ ਐਲਾਨਦੇ ਹਾਂ।

4
00:00:11,800 --> 00:00:15,200
ਇਹ ਰੀਲਿਜ਼ ਨਾਲ, ਗਨੋਮ ਨੇ ਸੋਧਿਆ ਹੋਇਆ ਡੈਸਕਟਾਪ ਪੇਸ਼ ਕੀਤਾ ਹੈ।

5
00:00:15,200 --> 00:00:16,900
..ਇਸ ਦੇ ਯੂਜ਼ਰ ਤੇ ਡਿਵੈਲਪਰਾਂ ਲਈ।

6
00:00:18,000 --> 00:00:20,700
ਗਨੋਮ ਵਿੱਚ ਦਿੱਖ ਵੇਰਵੇ ਨੂੰ ਸੋਧਿਆ ਗਿਆ ਹੈ..

7
00:00:20,800 --> 00:00:23,600
..ਤੇ ਨਵੀਆਂ ਐਨੀਮੇਸ਼ਨਾਂ ਨੂੰ ਲਾਗੂ ਕੀਤਾ ਗਿਆ ਹੈ..

8
00:00:23,600 --> 00:00:25,200
..ਤੁਹਾਡੇ ਯੂਜ਼ਰ ਤਜਰਬੇ ਨੂੰ ਸੁਧਾਰਨ ਲਈ।

9
00:00:26,300 --> 00:00:29,500
ਇਹ ਰੀਲਿਜ਼ ਵਿੱਚ ਮਲਟੀ-ਟੱਚ ਲਈ ਵਧੀਆ ਸਹਿਯੋਗ ਵਿੱਚ ਮਿਲਿਆ ਹੈ।

10
00:00:30,880 --> 00:00:32,900
ਤੁਸੀਂ ਹੁਣ ਸਕਰਾ ਅਤੇ ਜ਼ੂਮ ਕਰ ਸਕਦੇ ਹੋ..

11
00:00:32,900 --> 00:00:34,200
..ਗਨੋਮ ਐਪਸ ਦੇ ਮਲਟੀਟਿਊਡ ਵਿੱਚ

12
00:00:35,680 --> 00:00:38,200
ਗਨੋਮ ਸ਼ੈਲ ਵਿੱਚ ਨਵੀਂ ਜੈਸ਼ਚਰ ਹਨ।

13
00:00:38,720 --> 00:00:41,700
ਜਿਵੇਂ ਕਿ ਜਦੋਂ ਵਰਕਸਪੇਸ ਵਿੱਚ ਬਦਲਣਾ ਹੈ..

14
00:00:41,700 --> 00:00:44,200
..ਅਤੇ ਸਰਗਰਮੀ ਸਾਰ ਨੂੰ ਖੋਲ੍ਹਣਾ।

15
00:00:45,200 --> 00:00:47,700
ਗਨੋਮ ਕਮਿਊਨਟੀ ਦੀ ਮਦਦ ਨਾਲ..

16
00:00:47,700 --> 00:00:50,100
...ਇਹ ਸਾਲ ਦੀ ਇੰਟਰਨਸ਼ਿਪ ਦੇ ਭਾਗ ਲੈਣ ਵਾਲਿਆਂ ਤੋਂ..

17
00:00:50,100 --> 00:00:52,600
..ਕਈ ਗਨੋਮ ਐਪਸ ਵਿੱਚ ਅੱਪਡੇਟ ਦਿੱਤਾ ਗਿਆ ਹੈ।

18
00:00:54,000 --> 00:00:56,440
ਸਾਫਟਵੇਅਰ ਹੁਣ ਐਪ ਐਡ-ਆਨ ਲਈ ਸਹਾਇਕ ਹੈ।

19
00:00:57,000 --> 00:01:00,000
ਨਕਸ਼ੇ ਵਿੱਚ ਨੇਵੀਗੇਸ਼ਨ ਰੂਟਿੰਗ ਮਿਲਿਆ ਹੈ।

20
00:01:01,000 --> 00:01:03,420
ਫੋਟੋ ਗੂਗਲ ਤੋਂ ਤੁਹਾਡੀਆਂ ਫੋਟੋ ਬਰਾਮਦ ਕਰ ਸਕਦਾ ਹੈ।

21
00:01:04,000 --> 00:01:07,000
ਅਤੇ ਮੌਸਮ ਦੀ ਦਿੱਖ ਮੁੜ-ਤਿਆਰ ਕੀਤੀ ਗਈ ਸੀ।

22
00:01:08,800 --> 00:01:13,900
ਇਸ ਤੋਂ ਬਿਨਾਂ ਘੜੀਆਂ ਤੇ ਕੈਲਕੂਲੇਟਰ ਵਿੱਚ ਨਵੇਂ ਖੋਜ ਦੇਣ ਵਾਲੇ ਹਨ..

23
00:01:13,900 --> 00:01:15,800
..ਤਾਂ ਕਿ ਤੁਸੀਂ ਸਮਾਂ ਵੇਖ ਸਕੋ..

24
00:01:15,800 --> 00:01:17,800
..ਤੇ ਖੋਜ ਖੇਤਰ ਵਿੱਚ ਸਿੱਧੀ ਗਿਣਤੀ ਕਰ ਸਕੋ।

25
00:01:18,200 --> 00:01:21,400
ਸੂਕੁਡੋ ਅਤੇ ਸੁਰੰਗਾਂ ਦਾ ਰੰਗ-ਰੂਪ ਬਦਲਿਆ ਗਿਆ..

26
00:01:21,400 --> 00:01:22,960
..ਵਧੀਆ ਖੇਡ ਤਜਰਬਾ ਦੇਣ ਲਈ।

27
00:01:23,360 --> 00:01:25,200
ਅਤੇ ਨਵੀਂ ਖੇਡ, ਹਿਟੋਰੀ..

28
00:01:25,220 --> 00:01:27,800
..ਹੁਣ ਗਨੋਮ ਖੇਡਾਂ ਦਾ ਭਾਗ ਹੈ।

29
00:01:30,200 --> 00:01:33,000
ਗਨੋਮ ੩.੧੪ ਨਾਲ ਡਿਵੈਲਪਰ ਭਵਿੱਖ ਵਿੱਚ ਕਦਮ ਰੱਖ ਸਕਦੇ ਹਨ..

30
00:01:33,000 --> 00:01:35,000
..ਵਧੀਆ ਡਿਵੈਲਪਮੈਂਟ ਇੰਵਾਇਰਨਮੈਂਟ

31
00:01:35,700 --> 00:01:37,740
GTK ਨੇ ਜਾਰੀ ਰੱਖਣ ਵਾਲਾ ਯਤਨ ਸ਼ੁਰੂ ਕੀਤਾ ਹੈ.. 

32
00:01:37,740 --> 00:01:39,500
..ਡਿਵੈਲਪਰ ਤਜਰਬੇ ਨੂੰ ਵਧੀਆ ਬਣਾਉਣ ਲਈ।

33
00:01:40,860 --> 00:01:43,000
ਇਹ ਰੀਲਿਜ਼ ਵਿੱਚ GTK ਇੰਸਪੈਕਟਰ ਹੈ..

34
00:01:43,240 --> 00:01:46,940
ਤੁਹਾਡੇ ਡੀਬੱਗ ਵਰਕਫਲੋ ਨੂੰ ਸੁਧਾਰਨ ਲਈ ਦਿਲਖਿੱਚਵਾਂ ਡੀਬੱਗਰ ਹੈ।

35
00:01:50,560 --> 00:01:53,040
GTK ਵਿੱਚ ਹੋਰ ਵੀ ਵੱਧ ਦਸਤਾਵੇਜ਼ ਹਨ..

36
00:01:53,280 --> 00:01:56,520
..ਇਨਸਾਨੀ ਇੰਟਰਫੇਸ ਸੇਧਾਂ ਦੇ ਬਿਲਕੁਲ ਨਵੀਂ ਸੈੱਟ ਦੇ ਸਮੇਤ।

37
00:01:59,560 --> 00:02:01,800
GTK ਲਈ ਖ਼ਬਰਾਂ ਤੋਂ ਬਿਨਾਂ..

38
00:02:01,800 --> 00:02:03,400
..ਇਹ ਰੀਲਿਜ਼ ਵਿੱਚ ਹੋਰ ਸਥਿਰ ਮਿਲੀ ਹੈ..

39
00:02:03,400 --> 00:02:05,300
..ਵੇਅਲੈਂਡ ਵਿੱਚ ਗਨੋਮ ਕੰਪੋਜ਼ਟਰ ਲਈ।

40
00:02:06,300 --> 00:02:08,640
ਇਸ ਤੋਂ ਬਿਨਾਂ glib ਵਿੱਚ ਕਈ ਨਵੀਂ ਫੀਚਰ ਆਏ ਹਨ..

41
00:02:09,040 --> 00:02:10,860
...ਜਿਵੇਂ ਯੂਨੀਕੋਡ 7 ਦੇ ਅਨੁਕੂਲ..

42
00:02:10,960 --> 00:02:13,800
...ਅਤੇ ਨਵੀਆਂ ਮਾਈਮ-ਐਪਸ ਸੇਧਾਂ ਲਈ ਸਹਿਯੋਗ।

43
00:02:15,580 --> 00:02:18,800
ਭਵਿੱਖ ਵਿੱਚ ਕਈ ਡਿਸਟਰੀਬਿਊਸ਼ਨ ਗਨੋਮ ੩.੧੪ ਦੇਣਗੀਆਂ।

44
00:02:20,400 --> 00:02:21,780
ਜੇ ਤੁਸੀਂ ਉਡੀਕ ਨਹੀਂ ਕਰ ਸਕਦੇ ਤਾਂ...

45
00:02:22,020 --> 00:02:23,800
ਵੇਰਵੇ ਵਿੱਚ ਸਾਡੇ ਡੈਮੋ ਈਮੇਜ਼ ਨਾਲ ਕੋਸ਼ਿਸ਼ ਕਰਕੇ ਵੇਖੋ।

46
00:02:24,800 --> 00:02:27,800
ਜਾਂ ਸਾਡੇ ਅਗਲੇ ਗਨੋਮ ਰੀਲਿਜ਼ ਨੂੰ ਹੋਰ ਵੀ ਵਧੀਆ ਬਣਾਉਣ ਲਈ ਸਾਡੇ ਨਾਲ ਹੱਥ ਮਿਲਾਉ।

47
00:02:29,400 --> 00:02:31,900
ਅਸੀਂ ਤੁਹਾਡੇ ਸਾਥ ਦੇਣ ਲਈ ਉਡੀਕ ਵਿੱਚ ਹਾਂ।
