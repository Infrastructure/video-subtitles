1
00:00:04,000 --> 00:00:06,500
Thanks to the hard work of our contributors..

2
00:00:06,800 --> 00:00:08,000
..and thanks to the friends of GNOME

3
00:00:08,200 --> 00:00:11,000
..we're proud to announce GNOME 3.14.

4
00:00:11,800 --> 00:00:15,200
With this release GNOME brings an improved desktop..

5
00:00:15,200 --> 00:00:16,900
..for its users and developers.

6
00:00:18,000 --> 00:00:20,700
The visual details in GNOME have been revised..

7
00:00:20,800 --> 00:00:23,600
..and new animations have been implemented..

8
00:00:23,600 --> 00:00:25,200
..to improve your user experience.

9
00:00:26,300 --> 00:00:29,500
This release also brings better support for multitouch.

10
00:00:30,880 --> 00:00:32,900
You can now use swipe and zoom..

11
00:00:32,900 --> 00:00:34,200
..in a multitude of GNOME apps.

12
00:00:35,680 --> 00:00:38,200
GNOME Shell also has new gestures.

13
00:00:38,720 --> 00:00:41,700
For example, when switching workspaces..

14
00:00:41,700 --> 00:00:44,200
..and opening the activities overview.

15
00:00:45,200 --> 00:00:47,700
With help from GNOME's community..

16
00:00:47,700 --> 00:00:50,100
..and from this year's internship participants..

17
00:00:50,100 --> 00:00:52,600
..a lot of the GNOME apps have received updates.

18
00:00:54,000 --> 00:00:56,440
Software now supports app add-ons.

19
00:00:57,000 --> 00:01:00,000
Maps gained navigation routing.

20
00:01:01,000 --> 00:01:03,420
Photos can import your photos from Google.

21
00:01:04,000 --> 00:01:07,000
And Weather's interface was redesigned.

22
00:01:08,800 --> 00:01:13,900
Furthermore, clocks and calculator have new search providers..

23
00:01:13,900 --> 00:01:15,800
..so you can check the time..

24
00:01:15,800 --> 00:01:17,800
..and calculate live in the search field.

25
00:01:18,200 --> 00:01:21,400
Sudoku and mines were redesigned..

26
00:01:21,400 --> 00:01:22,960
..for a better gameplay experience.

27
00:01:23,360 --> 00:01:25,200
And a new game, named Hitori..

28
00:01:25,220 --> 00:01:27,800
..is now a part of GNOME games.

29
00:01:30,200 --> 00:01:33,000
With GNOME 3.14, developers can look forward to..

30
00:01:33,000 --> 00:01:35,000
..a better development environment.

31
00:01:35,700 --> 00:01:37,740
GTK has begun an ongoing effort.. 

32
00:01:37,740 --> 00:01:39,500
..to improve the developer experience.

33
00:01:40,860 --> 00:01:43,000
This release ships with GTK Inspector..

34
00:01:43,240 --> 00:01:46,940
an interactive debugger aimed to improve your debugging workflow.

35
00:01:50,560 --> 00:01:53,040
GTK also comes with more documentation..

36
00:01:53,280 --> 00:01:56,520
..including a brand new set of human interface guidelines.

37
00:01:59,560 --> 00:02:01,800
Apart from the news for GTK..

38
00:02:01,800 --> 00:02:03,400
..this release brings more stability..

39
00:02:03,400 --> 00:02:05,300
..to the GNOME compositor in Wayland.

40
00:02:06,300 --> 00:02:08,640
Furthermore, glib has gained new features..

41
00:02:09,040 --> 00:02:10,860
..like Unicode 7 compliance..

42
00:02:10,960 --> 00:02:13,800
..and support for the new Mimeapps spec.

43
00:02:15,580 --> 00:02:18,800
Many distributions will ship GNOME 3.14 in the future.

44
00:02:20,400 --> 00:02:21,780
If you can't wait..

45
00:02:22,020 --> 00:02:23,800
Try out our demo image in the description.

46
00:02:24,800 --> 00:02:27,800
Or come join us in making our next GNOME release even better. 

47
00:02:29,400 --> 00:02:31,900
We look forward to getting you started.

