1
00:00:04,000 --> 00:00:06,500
Gracias al duro trabajo de nuestros colaboradores..

2
00:00:06,800 --> 00:00:08,000
..y gracias a los amigos de GNOME..

3
00:00:08,200 --> 00:00:11,000
..nos complace anunciar GNOME 3.14.

4
00:00:11,800 --> 00:00:15,200
En esta versión, GNOME ofrece un escritorio mejorado..

5
00:00:15,200 --> 00:00:16,900
..para usuarios y desarrolladores.

6
00:00:18,000 --> 00:00:20,700
Se han revisado los detalles visuales de GNOME..

7
00:00:20,800 --> 00:00:23,600
..y se han añadido nuevas animaciones..

8
00:00:23,600 --> 00:00:25,200
..para mejorar la experiencia del usuario.

9
00:00:26,300 --> 00:00:29,500
Esta versión también incluye un mejor soporte de multitoque.

10
00:00:30,880 --> 00:00:32,900
Ahora puede deslizar y ampliar..

11
00:00:32,900 --> 00:00:34,200
..en muchas aplicaciones de GNOME.

12
00:00:35,680 --> 00:00:38,200
GNOME Shell también tiene gestos nuevos.

13
00:00:38,720 --> 00:00:41,700
Por ejemplo, al cambiar entre áreas de trabajo..

14
00:00:41,700 --> 00:00:44,200
..y abrir la vista de actividades.

15
00:00:45,200 --> 00:00:47,700
Con la ayuda de la comunidad de GNOME..

16
00:00:47,700 --> 00:00:50,100
..y de los participantes de las prácticas de este año..

17
00:00:50,100 --> 00:00:52,600
..se han actualizado muchas aplicaciones de GNOME.

18
00:00:54,000 --> 00:00:56,440
Software ahora soporta complementos de aplicaciones.

19
00:00:57,000 --> 00:01:00,000
Mapas incluye rutas de navegación.

20
00:01:01,000 --> 00:01:03,420
Fotos puede importar sus fotos de Google.

21
00:01:04,000 --> 00:01:07,000
Y se ha rediseñado la interfaz de Meteorología.

22
00:01:08,800 --> 00:01:13,900
Además, Relojes y la calculadora tienen nuevos proveedores de búsqueda..

23
00:01:13,900 --> 00:01:15,800
..por lo que puede ver la hora..

24
00:01:15,800 --> 00:01:17,800
..y calcular en directo desde la caja de búsqueda.

25
00:01:18,200 --> 00:01:21,400
Sudoku y Minas se han rediseñado..

26
00:01:21,400 --> 00:01:22,960
..para una mejor experiencia del juego.

27
00:01:23,360 --> 00:01:25,200
Y un nuevo juego, llamado Hitori..

28
00:01:25,220 --> 00:01:27,800
..es ahora parte de los juegos de GNOME.

29
00:01:30,200 --> 00:01:33,000
Con GNOME 3.14, lo desarrolladores pueden esperar..

30
00:01:33,000 --> 00:01:35,000
..un mejor entorno de desarrollo.

31
00:01:35,700 --> 00:01:37,740
GTK+ ha iniciado un esfuerzo..

32
00:01:37,740 --> 00:01:39,500
..para mejorar la experiencia del desarrollador.

33
00:01:40,860 --> 00:01:43,000
Esta versión incluye el inspector de GTK+..

34
00:01:43,240 --> 00:01:46,940
..un depurador interactivo para mejorar el proceso de depurado.

35
00:01:50,560 --> 00:01:53,040
GTK+ también incluye más documentación..

36
00:01:53,280 --> 00:01:56,520
..incluyendo un nuevo conjunto de guías de interfaces humanas.

37
00:01:59,560 --> 00:02:01,800
Aparte de las novedades en GTK+..

38
00:02:01,800 --> 00:02:03,400
..esta versión ofrece mayor estabilidad..

39
00:02:03,400 --> 00:02:05,300
..al compositor de GNOME en Wayland.

40
00:02:06,300 --> 00:02:08,640
Además, Glib ha obtenido nuevas características..

41
00:02:09,040 --> 00:02:10,860
..como el soporte de Unicode 7..

42
00:02:10,960 --> 00:02:13,800
.. y el soporte de la nueva especificación de tipos MIME de aplicaciones.

43
00:02:15,580 --> 00:02:18,800
Muchas distribuciones incluirán GNOME 3.14 en el futuro.

44
00:02:20,400 --> 00:02:21,780
Si no puede esperar..

45
00:02:22,020 --> 00:02:23,800
..pruebe nuestra imagen de pruebas en la descripción.

46
00:02:24,800 --> 00:02:27,800
O únase a nosotros para hacer nuestra próxima versión de GNOME aún mejor.

47
00:02:29,400 --> 00:02:31,900
Esperamos conseguir que lo pruebe.
