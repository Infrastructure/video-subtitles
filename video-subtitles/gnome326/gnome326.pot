#. extracted from gnome326.srt
#, fuzzy
msgid ""
msgstr ""
"Project-Id-Version: PACKAGE VERSION\n"
"Report-Msgid-Bugs-To: \n"
"POT-Creation-Date: 2017-09-14 12:08+1000\n"
"PO-Revision-Date: YEAR-MO-DA HO:MI+ZONE\n"
"Last-Translator: FULL NAME <EMAIL@ADDRESS>\n"
"Language-Team: LANGUAGE <LL@li.org>\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"X-Generator: Translate Toolkit 2.2.3\n"

#. visible for 2 seconds
#: 00:00:03.258-->00:00:05.800
msgid "GNOME 3.26 is here.."
msgstr ""

#. visible for 3 seconds
#: 00:00:05.800-->00:00:09.092
msgid "..packed by 6 months of exciting changes by the community."
msgstr ""

#. visible for 2 seconds
#: 00:00:10.008-->00:00:12.550
msgid "The shell is GNOME's user interface.."
msgstr ""

#. visible for 3 seconds
#: 00:00:13.383-->00:00:16.758
msgid "..and this cycle it has received a visual overhaul of its search,.."
msgstr ""

#. visible for 1 seconds
#: 00:00:16.842-->00:00:18.675
msgid "..giving you a better overview."
msgstr ""

#. visible for 2 seconds
#: 00:00:20.592-->00:00:23.550
msgid "GNOME's interface is configured from the control center,.."
msgstr ""

#. visible for 1 seconds
#: 00:00:23.550-->00:00:25.008
msgid "..which features a new sidebar."
msgstr ""

#. visible for 2 seconds
#: 00:00:25.217-->00:00:27.675
msgid "..making it possible to jump between settings easily.."
msgstr ""

#. visible for 1 seconds
#: 00:00:27.675-->00:00:29.133
msgid "..to find what you are looking for."
msgstr ""

#. visible for 2 seconds
#: 00:00:30.092-->00:00:32.425
msgid "A dedicated Wi-Fi area ensures easy.."
msgstr ""

#. visible for 2 seconds
#: 00:00:32.425-->00:00:35.217
msgid "..and logical access for managing wireless connections."
msgstr ""

#. visible for 2 seconds
#: 00:00:35.925-->00:00:38.383
msgid "The Display settings are quicker to access,.."
msgstr ""

#. visible for 1 seconds
#: 00:00:38.383-->00:00:39.842
msgid "..so you can configure common setups.."
msgstr ""

#. visible for 3 seconds
#: 00:00:39.842-->00:00:43.508
msgid ".. such as single displays, joint displays and mirror."
msgstr ""

#. visible for 2 seconds
#: 00:00:45.800-->00:00:48.675
msgid "Many improvements have also landed for GNOME Apps."
msgstr ""

#. visible for 3 seconds
#: 00:00:49.258-->00:00:52.508
msgid "Simple Scan has had a redesign of its editing tools."
msgstr ""

#. visible for 4 seconds
#: 00:00:53.217-->00:00:57.675
msgid "Polari now comes with an initial setup to get you started quickly.."
msgstr ""

#. visible for 3 seconds
#: 00:00:57.675-->00:01:00.717
msgid "..and has support for sending and receiving emojis."
msgstr ""

#. visible for 3 seconds
#: 00:01:02.133-->00:01:05.175
msgid "Logs gained event compression for better overview."
msgstr ""

#. visible for 4 seconds
#: 00:01:06.217-->00:01:10.425
msgid "Photos has new zoom controls,.."
msgstr ""

#. visible for 2 seconds
#: 00:01:10.425-->00:01:13.217
msgid "..and Disks gained the ability to resize partitions.."
msgstr ""

#. visible for 1 seconds
#: 00:01:13.217-->00:01:14.758
msgid "..and new disk image files."
msgstr ""

#. visible for 3 seconds
#: 00:01:18.133-->00:01:21.175
msgid "Developers can look forward to big additions in Builder."
msgstr ""

#. visible for 2 seconds
#: 00:01:21.175-->00:01:23.883
msgid "It features a new visual design and animations."
msgstr ""

#. visible for 1 seconds
#: 00:01:24.467-->00:01:26.425
msgid "An initial built-in debugger enables you.."
msgstr ""

#. visible for 2 seconds
#: 00:01:26.425-->00:01:28.758
msgid "..to debug application threads and add break points."
msgstr ""

#. visible for 1 seconds
#: 00:01:29.550-->00:01:30.758
msgid "..and much more."
msgstr ""

#. visible for 1 seconds
#: 00:01:33.842-->00:01:35.508
msgid "GNOME is a worldwide project.."
msgstr ""

#. visible for 2 seconds
#: 00:01:35.508-->00:01:38.342
msgid "..and with your help we can push software freedom to the next level."
msgstr ""

#. visible for 2 seconds
#: 00:01:39.383-->00:01:42.092
msgid "Have a chat with us or read more at gnome.org."
msgstr ""
