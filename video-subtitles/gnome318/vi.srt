1
00:00:04,040 --> 00:00:06,760
Cảm ơn sự làm việc chăm chỉ của những người trong dự án và những người đóng góp

2
00:00:06,760 --> 00:00:09,140
GNOME thông báo phiên bản 3.18.

3
00:00:10,160 --> 00:00:13,560
Trong bản phát hành này, GNOME tăng cường trải nghiệm..

4
00:00:13,560 --> 00:00:15,560
..cho người dùng cũng như cho nhà phát triển.

5
00:00:17,200 --> 00:00:18,860
Nếu máy tính của bạn có cảm biến ánh sáng..

6
00:00:19,160 --> 00:00:21,160
..GNOME giờ có thể tự động dùng nó..

7
00:00:21,160 --> 00:00:23,120
..để điều chỉnh độ sáng của màn hình.

8
00:00:24,080 --> 00:00:26,520
Trải nghiệm màn hình cảm ứng cũng được tăng cường..

9
00:00:26,600 --> 00:00:29,040
..khi chọn hay sửa văn bản.

10
00:00:30,760 --> 00:00:33,560
Các tập tin nhận được nhiều sự quan tâm trong chu kỳ này.

11
00:00:34,320 --> 00:00:37,520
Có cách trình bày mới ở khung bên do vậy các vị trí nội bộ cũng như trên mạng..

12
00:00:37,520 --> 00:00:40,280
..được quy vào một mối để phục vụ bạn.

13
00:00:40,960 --> 00:00:42,960
Tiến triển công việc giờ được hiển thị ở thanh đầu..

14
00:00:42,960 --> 00:00:44,960
..khi các tập tin đang được sao chép.

15
00:00:45,400 --> 00:00:47,000
Nhiều thứ khác được cải thiện..

16
00:00:47,320 --> 00:00:49,320
..bao gồm tính năng tìm kiếm tao nhã..

17
00:00:50,520 --> 00:00:52,520
..kết hợp Google Drive..

18
00:00:52,520 --> 00:00:54,520
..và quản lý tập tin tốt hơn.

19
00:00:58,160 --> 00:01:01,400
Các phần mềm trở thành thứ đầu tiên hỗ trợ cập nhật firmware thiết bị..

20
00:01:01,400 --> 00:01:04,480
..thông qua Linux Vendor Firmware Service mới.

21
00:01:05,480 --> 00:01:08,200
Dịch vụ cho phép các nhà cung cấp gửi firmware..

22
00:01:08,200 --> 00:01:11,880
..và GNOME Software sẽ cho phép bạn cài đặt nó chỉ với một cú bấm chuột.

23
00:01:14,240 --> 00:01:16,720
Hơn thế nữa, cộng đồng GNOME..

24
00:01:16,720 --> 00:01:19,280
..đã cải thiện một lượng lớn các ứng dụng khác nhau..

25
00:01:19,280 --> 00:01:20,760
..bao gồm Lịch...

26
00:01:20,760 --> 00:01:23,120
..cái mà giờ đến cùng với hộp thoại quản lý..

27
00:01:23,120 --> 00:01:24,760
..để giúp bạn quản lý lịch của mình.

28
00:01:25,400 --> 00:01:26,680
Polari..

29
00:01:26,680 --> 00:01:29,280
..với một trải nghiệm người dùng khởi đầu tốt hơn.

30
00:01:30,280 --> 00:01:31,560
Tài liệu GNOME..

31
00:01:31,560 --> 00:01:35,360
..một ứng dụng có giao diện người dùng để tổ chức tài liệu của bạn.

32
00:01:35,760 --> 00:01:36,640
Và Evince..

33
00:01:36,640 --> 00:01:39,440
.. cái mà hỗ trợ ghi chú và phim được cải tiến.

34
00:01:43,080 --> 00:01:46,200
GNOME 3.18 đến cùng với xem trước GNOME To Do..

35
00:01:46,200 --> 00:01:50,520
..bộ quản lý công tác cá nhân giúp bạn theo dõi dấu vết các công tác sau này. 

36
00:01:52,040 --> 00:01:55,120
Các công cụ phát triển của GNOME đồng thời cũng được cải thiện trong chu kỳ này.

37
00:01:55,440 --> 00:01:57,040
Builder, ví dụ thế..

38
00:01:57,040 --> 00:01:58,960
..giờ có minimap mã nguồn..

39
00:01:59,000 --> 00:02:00,680
..tổng quan về phím tắt..

40
00:02:00,680 --> 00:02:02,480
.. và tính năng tự hoàn thiện nốt các từ cho Python.

41
00:02:03,280 --> 00:02:06,400
Hướng dẫn giao diện người dùng giờ được tái cấu trúc lại..

42
00:02:06,400 --> 00:02:08,400
..và đưa thêm vào các mẫu cách dùng mới.

43
00:02:09,440 --> 00:02:12,840
Giao diện người dùng của Xem nhật ký GNOME đưa ra ngắn gọn hơn..

44
00:02:12,920 --> 00:02:15,400
..và có thể cho bạn xem nhật ký của 5 lần khởi động cuối.

45
00:02:16,240 --> 00:02:18,320
Boxes đã được cải tiến giao diện của nó..

46
00:02:18,320 --> 00:02:19,680
..với cách trình bày danh sách mới..

47
00:02:19,680 --> 00:02:21,960
..cho những người với nhiều máy ảo.

48
00:02:22,280 --> 00:02:25,960
Cuối cùng, GTK+ đã nhận tính năng typographic mới.

49
00:02:28,280 --> 00:02:30,600
Bản phát hành GNOME mới giờ đã sẵn có cho bạn..

50
00:02:30,680 --> 00:02:32,520
..ở dạng ảnh live để thử.

51
00:02:33,600 --> 00:02:37,400
Nhiều bản phân phối cũng sẽ sớm xuất xưởng GNOME 3.18 trong tương lai gần.

52
00:02:39,360 --> 00:02:41,520
GNOME là một cộng đồng thân thiện..

53
00:02:41,520 --> 00:02:43,200
..những người tạo phần mềm miễn phí cho mọi người.

54
00:02:43,960 --> 00:02:44,900
Hay quá phải không?

55
00:02:45,280 --> 00:02:48,500
Hãy gia nhập với chúng tôi để GNOME trở môi trường máy tính để bàn hoàn hảo.
