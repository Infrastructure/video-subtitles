1
00:00:04,040 --> 00:00:06,760
Dzięki ciężkiej pracy stażystów i współtwórców

2
00:00:06,760 --> 00:00:09,140
GNOME ogłasza wersję 3.18.

3
00:00:10,160 --> 00:00:13,560
W tym wydaniu ulepszono interfejs GNOME

4
00:00:13,560 --> 00:00:15,560
dla użytkowników i programistów.

5
00:00:17,200 --> 00:00:18,860
Jeśli komputer ma czujnik oświetlenia

6
00:00:19,160 --> 00:00:21,160
GNOME użyje go automatycznie

7
00:00:21,160 --> 00:00:23,120
do dostosowania jasności ekranu.

8
00:00:24,080 --> 00:00:26,520
Ulepszono także obsługę ekranów dotykowych

9
00:00:26,600 --> 00:00:29,040
podczas zaznaczania i modyfikowania tekstu.

10
00:00:30,760 --> 00:00:33,560
W tym cyklu Menedżer plików uległ wielu zmianom.

11
00:00:34,320 --> 00:00:37,520
Na pasku bocznym znajduje się nowy widok zawierający

12
00:00:37,520 --> 00:00:40,280
położenia zdalne i wewnętrzne w jednym miejscu.

13
00:00:40,960 --> 00:00:42,960
Postęp kopiowania plików

14
00:00:42,960 --> 00:00:44,960
jest wyświetlany na pasku nagłówka.

15
00:00:45,400 --> 00:00:47,000
Inne ulepszenia

16
00:00:47,320 --> 00:00:49,320
to usprawnione wyszukiwanie

17
00:00:50,520 --> 00:00:52,520
integracja z serwisem Google Drive

18
00:00:52,520 --> 00:00:54,520
i lepsze zarządzanie plikami.

19
00:00:58,160 --> 00:01:01,400
Menedżer oprogramowania jako pierwszy obsługuje aktualizowanie

20
00:01:01,400 --> 00:01:04,480
oprogramowania sprzętowego przez witrynę „Linux Vendor Firmware Service”.

21
00:01:05,480 --> 00:01:08,200
Ta witryna umożliwia producentom na wysyłanie aktualizacji

22
00:01:08,200 --> 00:01:11,880
a Menedżer oprogramowania GNOME umożliwia ich zainstalowanie jednym kliknięciem.

23
00:01:14,240 --> 00:01:16,720
Społeczność GNOME

24
00:01:16,720 --> 00:01:19,280
ulepszyła liczne programy.

25
00:01:19,280 --> 00:01:20,760
Kalendarz

26
00:01:20,760 --> 00:01:23,120
zawiera teraz okno zarządzania

27
00:01:23,120 --> 00:01:24,760
pomagające w obsłudze kalendarzy.

28
00:01:25,400 --> 00:01:26,680
Polari

29
00:01:26,680 --> 00:01:29,280
posiada lepszy interfejs pierwszego uruchomienia.

30
00:01:30,280 --> 00:01:31,560
Menedżer dokumentów GNOME

31
00:01:31,560 --> 00:01:35,360
zawiera zmieniony interfejs do organizowania dokumentów.

32
00:01:35,760 --> 00:01:36,640
A w Evince

33
00:01:36,640 --> 00:01:39,440
ulepszono obsługę adnotacji i nagrań wideo.

34
00:01:43,080 --> 00:01:46,200
GNOME 3.18 zawiera wersję beta Menedżera zadań GNOME

35
00:01:46,200 --> 00:01:50,520
pomagającego w planowaniu zadań.

36
00:01:52,040 --> 00:01:55,120
W tym cyklu ulepszono także narzędzia programistyczne GNOME.

37
00:01:55,440 --> 00:01:57,040
Na przykład, Builder

38
00:01:57,040 --> 00:01:58,960
posiada teraz minimapę kodu źródłowego

39
00:01:59,000 --> 00:02:00,680
podgląd skrótów

40
00:02:00,680 --> 00:02:02,480
i automatyczne uzupełnianie w języku Python.

41
00:02:03,280 --> 00:02:06,400
Wytyczne projektowania interfejsu zostały zmienione

42
00:02:06,400 --> 00:02:08,400
i zawierają nowe wzorce.

43
00:02:09,440 --> 00:02:12,840
Interfejs Dzienników GNOME daje teraz lepszy przegląd

44
00:02:12,920 --> 00:02:15,400
i może wyświetlać dzienniki z pięciu ostatnich uruchomień.

45
00:02:16,240 --> 00:02:18,320
Ulepszono interfejs Boxes

46
00:02:18,320 --> 00:02:19,680
dodając nowy widok listy

47
00:02:19,680 --> 00:02:21,960
dla użytkowników z wieloma maszynami wirtualnymi.

48
00:02:22,280 --> 00:02:25,960
GTK+ otrzymało nowe funkcje typograficzne.

49
00:02:28,280 --> 00:02:30,600
Nowe wydanie GNOME jest dostępne

50
00:02:30,680 --> 00:02:32,520
jako obraz Live do wypróbowania.

51
00:02:33,600 --> 00:02:37,400
W przyszłości wiele dystrybucji będzie zawierało GNOME 3.18.

52
00:02:39,360 --> 00:02:41,520
GNOME jest przyjazną społecznością

53
00:02:41,520 --> 00:02:43,200
tworzącą wolne oprogramowania dla każdego.

54
00:02:43,960 --> 00:02:44,900
Nie możesz się doczekać?

55
00:02:45,280 --> 00:02:48,500
Pomóż nam udoskonalić środowisko GNOME.
