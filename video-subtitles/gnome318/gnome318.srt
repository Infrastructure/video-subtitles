1
00:00:04,040 --> 00:00:06,760
Thanks to the hard work of interns and contributors

2
00:00:06,760 --> 00:00:09,140
GNOME announces version 3.18.

3
00:00:10,160 --> 00:00:13,560
In this release, GNOME improves the general user experience..

4
00:00:13,560 --> 00:00:15,560
..for users and new developers alike.

5
00:00:17,200 --> 00:00:18,860
If your computer has a light sensor..

6
00:00:19,160 --> 00:00:21,160
..GNOME can now use it to automatically..

7
00:00:21,160 --> 00:00:23,120
..adjust the brightness of your screen.

8
00:00:24,080 --> 00:00:26,520
The touch screen experience has also improved..

9
00:00:26,600 --> 00:00:29,040
..when selecting and modifying text.

10
00:00:30,760 --> 00:00:33,560
Files has received a lot of attention this cycle.

11
00:00:34,320 --> 00:00:37,520
There's a new view in the sidebar so remote and internal locations..

12
00:00:37,520 --> 00:00:40,280
..are collected in one place for your convience.

13
00:00:40,960 --> 00:00:42,960
Progress is now shown in the headerbar..

14
00:00:42,960 --> 00:00:44,960
..when files are being copied.

15
00:00:45,400 --> 00:00:47,000
There are many other improvements..

16
00:00:47,320 --> 00:00:49,320
..including a more polished search function..

17
00:00:50,520 --> 00:00:52,520
..Google Drive integration..

18
00:00:52,520 --> 00:00:54,520
..and better file management.

19
00:00:58,160 --> 00:01:01,400
Software has become the first to support updating device firmware..

20
00:01:01,400 --> 00:01:04,480
..through the new Linux Vendor Firmware Service.

21
00:01:05,480 --> 00:01:08,200
The service allows vendors to submit firmware..

22
00:01:08,200 --> 00:01:11,880
..and GNOME Software will allow you to install it with the click of a button.

23
00:01:14,240 --> 00:01:16,720
Furthermore, the GNOME community..

24
00:01:16,720 --> 00:01:19,280
..has improved a large variety of apps..

25
00:01:19,280 --> 00:01:20,760
..including Calendar..

26
00:01:20,760 --> 00:01:23,120
..which now comes with a management dialog..

27
00:01:23,120 --> 00:01:24,760
..to help you manage your calendars.

28
00:01:25,400 --> 00:01:26,680
Polari..

29
00:01:26,680 --> 00:01:29,280
..with a better initial user experience.

30
00:01:30,280 --> 00:01:31,560
GNOME Documents..

31
00:01:31,560 --> 00:01:35,360
..which is sporting a revamped user interface for organizing your documents.

32
00:01:35,760 --> 00:01:36,640
And Evince..

33
00:01:36,640 --> 00:01:39,440
.. which annotation and video support has improved.

34
00:01:43,080 --> 00:01:46,200
GNOME 3.18 comes with a preview of GNOME To Do..

35
00:01:46,200 --> 00:01:50,520
..a personal task manager helping you keep track of the tasks ahead of time.

36
00:01:52,040 --> 00:01:55,120
GNOME's development tools have also improved this cycle.

37
00:01:55,440 --> 00:01:57,040
Builder, for example..

38
00:01:57,040 --> 00:01:58,960
..now has a source code minimap..

39
00:01:59,000 --> 00:02:00,680
.. a shortcut overview..

40
00:02:00,680 --> 00:02:02,480
..and auto-completion for Python.

41
00:02:03,280 --> 00:02:06,400
The human interface guidelines have been restructured..

42
00:02:06,400 --> 00:02:08,400
..and include new usage patterns.

43
00:02:09,440 --> 00:02:12,840
GNOME Logs' user interface now gives a better overview..

44
00:02:12,920 --> 00:02:15,400
..and can show you logs from the last five boots.

45
00:02:16,240 --> 00:02:18,320
Boxes has improved its interface..

46
00:02:18,320 --> 00:02:19,680
..with a new list view..

47
00:02:19,680 --> 00:02:21,960
..for users with a lot of virtual machines.

48
00:02:22,280 --> 00:02:25,960
Finally, GTK+ has received new typographic features.

49
00:02:28,280 --> 00:02:30,600
The new GNOME release is available for you..

50
00:02:30,680 --> 00:02:32,520
..as a live image to try now.

51
00:02:33,600 --> 00:02:37,400
Many distributions will also ship GNOME 3.18 in the near future.

52
00:02:39,360 --> 00:02:41,520
GNOME is a friendly community..

53
00:02:41,520 --> 00:02:43,200
..who is making free software for everyone.

54
00:02:43,960 --> 00:02:44,900
Excited?

55
00:02:45,280 --> 00:02:48,500
Join us in making GNOME the perfect desktop environment.

