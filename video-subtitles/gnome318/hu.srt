1
00:00:04,040 --> 00:00:06,760
Köszönjük a gyakornokok és együttműködők kemény munkáját

2
00:00:06,760 --> 00:00:09,140
A GNOME bejelenti a 3.18-as verziót.

3
00:00:10,160 --> 00:00:13,560
Ebben a kiadásban a GNOME javítja az általános felhasználói élményt…

4
00:00:13,560 --> 00:00:15,560
…a felhasználók és új fejlesztők számára egyaránt.

5
00:00:17,200 --> 00:00:18,860
Ha a számítógépe rendelkezik fényérzékelővel…

6
00:00:19,160 --> 00:00:21,160
…akkor a GNOME mostantól képes használni azt…

7
00:00:21,160 --> 00:00:23,120
…a képernyő fényerejének automatikus beállításához.

8
00:00:24,080 --> 00:00:26,520
Az érintőképernyő élménye is továbbfejlesztésre került…

9
00:00:26,600 --> 00:00:29,040
…szöveg kijelölésekor és módosításakor.

10
00:00:30,760 --> 00:00:33,560
A Fájlok is sok figyelmet kapott ebben a ciklusban.

11
00:00:34,320 --> 00:00:37,520
Egy új nézet került az oldalsávra, így a távoli és a belső helyek…

12
00:00:37,520 --> 00:00:40,280
…egy helyre lettek összegyűjtve az Ön kényelmére.

13
00:00:40,960 --> 00:00:42,960
A folyamat mostantól a fejlécsávon látható…

14
00:00:42,960 --> 00:00:44,960
…a fájlok másolása közben.

15
00:00:45,400 --> 00:00:47,000
Sok más egyéb fejlesztést is van…

16
00:00:47,320 --> 00:00:49,320
…beleértve egy letisztultabb keresőfunkciót…

17
00:00:50,520 --> 00:00:52,520
…Google Drive integrációt…

18
00:00:52,520 --> 00:00:54,520
…és jobb fájlkezelést.

19
00:00:58,160 --> 00:01:01,400
A Szoftverek lett az első az eszköz firmware-frissítés támogatásában…

20
00:01:01,400 --> 00:01:04,480
…az új Linux gyártói firmware-szolgáltatáson keresztül.

21
00:01:05,480 --> 00:01:08,200
Ez a szolgáltatás firmware beküldését teszi lehetővé a gyártóknak…

22
00:01:08,200 --> 00:01:11,880
…és a GNOME Szoftverek lehetővé fogja tenni annak telepítését egyetlen kattintással.

23
00:01:14,240 --> 00:01:16,720
Továbbá a GNOME közösség…

24
00:01:16,720 --> 00:01:19,280
…továbbfejlesztett sokféle alkalmazást…

25
00:01:19,280 --> 00:01:20,760
…beleértve a Naptárat…

26
00:01:20,760 --> 00:01:23,120
…amely most egy kezelés párbeszédablakkal érkezik…

27
00:01:23,120 --> 00:01:24,760
…a naptárak kezelésének segítségére.

28
00:01:25,400 --> 00:01:26,680
Polari…

29
00:01:26,680 --> 00:01:29,280
…egy jobb kezdeti felhasználói élménnyel.

30
00:01:30,280 --> 00:01:31,560
GNOME Dokumentumok…

31
00:01:31,560 --> 00:01:35,360
…amely átalakított felhasználói felülettel érkezik a dokumentumok rendezéséhez.

32
00:01:35,760 --> 00:01:36,640
És az Evince…

33
00:01:36,640 --> 00:01:39,440
…amelynek magyarázat- és videotámogatása továbbfejlesztésre került.

34
00:01:43,080 --> 00:01:46,200
A GNOME 3.18 a GNOME Teendő előzetesével érkezik…

35
00:01:46,200 --> 00:01:50,520
…egy személyes feladatkezelő az idő előtti feladatok nyomon követésének segítéséhez.

36
00:01:52,040 --> 00:01:55,120
A GNOME fejlesztőeszközei is fejlesztésre kerültek ebben a ciklusban.

37
00:01:55,440 --> 00:01:57,040
Például az Építő…

38
00:01:57,040 --> 00:01:58,960
…mostantól forráskód minitérképpel rendelkezik…

39
00:01:59,000 --> 00:02:00,680
…egy gyorsbillentyű áttekintővel…

40
00:02:00,680 --> 00:02:02,480
…és automatikus kiegészítéssel a Pythonhoz.

41
00:02:03,280 --> 00:02:06,400
A felhasználóifelület-irányelvek átalakításra kerültek…

42
00:02:06,400 --> 00:02:08,400
…és új használati mintákat tartalmaznak.

43
00:02:09,440 --> 00:02:12,840
A GNOME Naplók felhasználói felülete jobb áttekintőt kapott…

44
00:02:12,920 --> 00:02:15,400
…és a legutolsó öt indítás naplóját is képes megjeleníteni.

45
00:02:16,240 --> 00:02:18,320
A Gépek felülete is tovább lett fejlesztve…

46
00:02:18,320 --> 00:02:19,680
…egy új listanézettel…

47
00:02:19,680 --> 00:02:21,960
…a számos virtuális géppel rendelkező felhasználóknak.

48
00:02:22,280 --> 00:02:25,960
Végül a GTK+ új tipográfiai szolgáltatásokat kapott.

49
00:02:28,280 --> 00:02:30,600
Az új GNOME kiadás elérhető az Ön számára…

50
00:02:30,680 --> 00:02:32,520
…egy live lemezképként, hogy kipróbálhassa most.

51
00:02:33,600 --> 00:02:37,400
Sok disztribúció is hamarosan szállítani fogja a GNOME 3.18-at a közeljövőben.

52
00:02:39,360 --> 00:02:41,520
A GNOME egy olyan barátságos közösség…

53
00:02:41,520 --> 00:02:43,200
…amely szabad szoftvereket készít bárki számára.

54
00:02:43,960 --> 00:02:44,900
Izgatott?

55
00:02:45,280 --> 00:02:48,500
Csatlakozzon hozzánk, tegye a GNOME-ot tökéletes asztali környezetté.
