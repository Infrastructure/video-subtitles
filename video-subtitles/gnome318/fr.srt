1
00:00:04,040 --> 00:00:06,760
Grâce au travail acharné de ses stagiaires et contributeurs

2
00:00:06,760 --> 00:00:09,140
GNOME annonce sa version 3.18.

3
00:00:10,160 --> 00:00:13,560
Dans cette version, GNOME améliore l'expérience utilisateur générale

4
00:00:13,560 --> 00:00:15,560
aussi bien pour les utilisateurs que pour les nouveaux développeurs.

5
00:00:17,200 --> 00:00:18,860
Si votre ordinateur a un capteur de luminosité ambiante

6
00:00:19,160 --> 00:00:21,160
GNOME peut désormais l'utiliser

7
00:00:21,160 --> 00:00:23,120
pour ajuster automatiquement la luminosité de l'écran

8
00:00:24,080 --> 00:00:26,520
L'expérience avec un écran tactile a aussi été améliorée

9
00:00:26,600 --> 00:00:29,040
lors de la sélection et la modification de texte.

10
00:00:30,760 --> 00:00:33,560
Fichiers a été au centre des attentions ce cycle.

11
00:00:34,320 --> 00:00:37,520
Une nouvelle vue dans la barre latérale rassemble

12
00:00:37,520 --> 00:00:40,280
emplacements distants et internes au même endroit.

13
00:00:40,960 --> 00:00:42,960
L'avancée est maintenant affichée dans la barre d'en-tête

14
00:00:42,960 --> 00:00:44,960
lorsque des fichiers sont copiés.

15
00:00:45,400 --> 00:00:47,000
Il y a de nombreuses autres améliorations

16
00:00:47,320 --> 00:00:49,320
y compris une fonction de recherche peaufinée,

17
00:00:50,520 --> 00:00:52,520
l'intégration de Google Drive

18
00:00:52,520 --> 00:00:54,520
et une meilleure gestion des fichiers.

19
00:00:58,160 --> 00:01:01,400
Logiciels est le premier endroit où vous pourrez mettre à jour vos micro-logiciels

20
00:01:01,400 --> 00:01:04,480
via le Linux Vendor Firmware Service

21
00:01:05,480 --> 00:01:08,200
Ce service permet aux fabricants de proposer leurs micro-logiciels

22
00:01:08,200 --> 00:01:11,880
et GNOME Logiciels peut ainsi proposer l'installation en un clic. 

23
00:01:14,240 --> 00:01:16,720
Par ailleurs, la communauté GNOME

24
00:01:16,720 --> 00:01:19,280
a amélioré une grande variété d'applications

25
00:01:19,280 --> 00:01:20,760
dont Agenda

26
00:01:20,760 --> 00:01:23,120
qui possède désormais un nouveau dialogue

27
00:01:23,120 --> 00:01:24,760
pour gérer vos agendas,

28
00:01:25,400 --> 00:01:26,680
Polari

29
00:01:26,680 --> 00:01:29,280
avec une meilleure première expérience,

30
00:01:30,280 --> 00:01:31,560
GNOME Documents

31
00:01:31,560 --> 00:01:35,360
et son interface revue pour l'organisation de vos documents,

32
00:01:35,760 --> 00:01:36,640
et enfin Evince

33
00:01:36,640 --> 00:01:39,440
dont la prise en charge des annotations et des vidéos a été amélioré.

34
00:01:43,080 --> 00:01:46,200
GNOME 3.18 comprend une pré-version de GNOME Tâches,

35
00:01:46,200 --> 00:01:50,520
un gestionnaire de tâches personnelles qui vous aide à vous organiser.

36
00:01:52,040 --> 00:01:55,120
Les outils de développement de GNOME ne sont pas en reste ce cycle.

37
00:01:55,440 --> 00:01:57,040
Builder, par exemple,

38
00:01:57,040 --> 00:01:58,960
a une vue d'ensemble du code,

39
00:01:59,000 --> 00:02:00,680
des raccourcis clavier

40
00:02:00,680 --> 00:02:02,480
et le complètement automatique pour Python.

41
00:02:03,280 --> 00:02:06,400
Les recommandations en matière d'interface utilisateur ont été restructurée

42
00:02:06,400 --> 00:02:08,400
et suggèrent de nouveaux patrons.

43
00:02:09,440 --> 00:02:12,840
L'interface de GNOME Journaux donne une meilleure vue d'ensemble

44
00:02:12,920 --> 00:02:15,400
et peut maintenant afficher les journaux des cinq derniers démarrages.

45
00:02:16,240 --> 00:02:18,320
Machines a une meilleure interface

46
00:02:18,320 --> 00:02:19,680
avec une vue en liste

47
00:02:19,680 --> 00:02:21,960
pour les utilisateurs qui ont de nombreuses machines.

48
00:02:22,280 --> 00:02:25,960
Enfin, GTK+ a de nouvelles fonctionnalités de typographie.

49
00:02:28,280 --> 00:02:30,600
La nouvelle version de GNOME est disponible

50
00:02:30,680 --> 00:02:32,520
sous la forme d'une image live.

51
00:02:33,600 --> 00:02:37,400
De nombreuses distributions fourniront aussi GNOME 3.18 prochainement.

52
00:02:39,360 --> 00:02:41,520
GNOME est une communauté accueillante

53
00:02:41,520 --> 00:02:43,200
qui produit du logiciel libre pour tous.

54
00:02:43,960 --> 00:02:44,900
Intéressé ?

55
00:02:45,280 --> 00:02:48,500
Rejoignez-nous et faisons de GNOME l'environnement de bureau parfait.
