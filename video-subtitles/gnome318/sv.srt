1
00:00:04,040 --> 00:00:06,760
Tack vare praktikanters och bidragsgivares hårda arbete

2
00:00:06,760 --> 00:00:09,140
annonserar GNOME version 3.18.

3
00:00:10,160 --> 00:00:13,560
I denna utgåva förbättrar GNOME den allmänna användarupplevelsen…

4
00:00:13,560 --> 00:00:15,560
…för såväl användare som nya utvecklare.

5
00:00:17,200 --> 00:00:18,860
Om din dator har en ljussensor…

6
00:00:19,160 --> 00:00:21,160
…kan GNOME nu använda den för att automatiskt…

7
00:00:21,160 --> 00:00:23,120
…justera ljusstyrkan hos din skärm.

8
00:00:24,080 --> 00:00:26,520
Pekskärmsupplevelsen har också förbättrats…

9
00:00:26,600 --> 00:00:29,040
…vid markering och ändring av text.

10
00:00:30,760 --> 00:00:33,560
Filer har fått mycket uppmärksamhet under denna cykel.

11
00:00:34,320 --> 00:00:37,520
Det finns en ny vy i sidopanelen så fjärrplatser och lokala platser…

12
00:00:37,520 --> 00:00:40,280
…samlas på ett ställe för din bekvämlighets skull.

13
00:00:40,960 --> 00:00:42,960
Förlopp visas nu i rubrikraden…

14
00:00:42,960 --> 00:00:44,960
…när filer kopieras.

15
00:00:45,400 --> 00:00:47,000
Det finns många andra förbättringar…

16
00:00:47,320 --> 00:00:49,320
…inkluderande en mer polerad sökfunktion…

17
00:00:50,520 --> 00:00:52,520
…integration med Google Drive…

18
00:00:52,520 --> 00:00:54,520
…och bättre filhantering.

19
00:00:58,160 --> 00:01:01,400
Programvara är först ut med att stödja uppdatering av fast programvara för enheter…

20
00:01:01,400 --> 00:01:04,480
…genom det nya Linux Vendor Firmware Service.

21
00:01:05,480 --> 00:01:08,200
Tjänsten låter tillverkare sända in fast programvara…

22
00:01:08,200 --> 00:01:11,880
…så kommer GNOME Programvara låta dig installera den med en knapptryckning.

23
00:01:14,240 --> 00:01:16,720
Vidare har GNOME-gemenskapen…

24
00:01:16,720 --> 00:01:19,280
…förbättrat ett stort antal program…

25
00:01:19,280 --> 00:01:20,760
…som Kalender…

26
00:01:20,760 --> 00:01:23,120
…som nu kommer med en hanteringsdialog…

27
00:01:23,120 --> 00:01:24,760
…för att hjälpa dig hantera dina kalendrar.

28
00:01:25,400 --> 00:01:26,680
Polari…

29
00:01:26,680 --> 00:01:29,280
…med en bättre inledande användarupplevelse.

30
00:01:30,280 --> 00:01:31,560
GNOME Dokument…

31
00:01:31,560 --> 00:01:35,360
…som ståtar med ett omarbetat användargränssnitt för att organisera dina dokument.

32
00:01:35,760 --> 00:01:36,640
Och Evince…

33
00:01:36,640 --> 00:01:39,440
…vars kommentars- och videostöd har förbättrats.

34
00:01:43,080 --> 00:01:46,200
GNOME 3.18 kommer med en förhandsversion av GNOME Uppgifter…

35
00:01:46,200 --> 00:01:50,520
…en uppgiftshanterare som hjälper dig hålla koll på uppgifter som ska göras senare.

36
00:01:52,040 --> 00:01:55,120
GNOME:s utvecklingsverktyg har också förbättrats under denna cykel.

37
00:01:55,440 --> 00:01:57,040
Builder har nu till exempel…

38
00:01:57,040 --> 00:01:58,960
…miniatyrer för källkod…

39
00:01:59,000 --> 00:02:00,680
…en översikt över snabbtangenter…

40
00:02:00,680 --> 00:02:02,480
…och automatisk komplettering för Python.

41
00:02:03,280 --> 00:02:06,400
Riktlinjerna för mänskliga gränssnitt har omstrukturerats…

42
00:02:06,400 --> 00:02:08,400
…och inkluderar nya användningsmönster.

43
00:02:09,440 --> 00:02:12,840
GNOME Loggars användargränssnitt erbjuder nu en bättre översikt…

44
00:02:12,920 --> 00:02:15,400
…och kan visa dig loggar från de senaste fem uppstarterna.

45
00:02:16,240 --> 00:02:18,320
Boxes har fått ett förbättrat gränssnitt…

46
00:02:18,320 --> 00:02:19,680
…med en ny listvy…

47
00:02:19,680 --> 00:02:21,960
…för användare med många virtuella maskiner.

48
00:02:22,280 --> 00:02:25,960
Slutligen har GTK+ erhållit nya typografiska funktioner.

49
00:02:28,280 --> 00:02:30,600
Den nya GNOME-utgåvan är tillgänglig för dig…

50
00:02:30,680 --> 00:02:32,520
…som en live-avbild att testa nu.

51
00:02:33,600 --> 00:02:37,400
Många distributioner kommer också att leverera GNOME 3.18 i en nära framtid.

52
00:02:39,360 --> 00:02:41,520
GNOME är en vänlig gemenskap…

53
00:02:41,520 --> 00:02:43,200
…som gör fri programvara för alla.

54
00:02:43,960 --> 00:02:44,900
Ivrig?

55
00:02:45,280 --> 00:02:48,500
Hjälp oss att göra GNOME den perfekta skrivbordsmiljön.
