1
00:00:04,040 --> 00:00:06,760
Graças aos esforços de estagiários e colaboradores

2
00:00:06,760 --> 00:00:09,140
A GNOME anuncia a versão 3.18.

3
00:00:10,160 --> 00:00:13,560
Neste lançamento, o GNOME melhora a experiência geral do usuário..

4
00:00:13,560 --> 00:00:15,560
..tanto para usuários como também para desenvolvedores.

5
00:00:17,200 --> 00:00:18,860
Se o seu computador possui um sensor de luz..

6
00:00:19,160 --> 00:00:21,160
..o GNOME pode, agora, utilizá-lo automaticamente..

7
00:00:21,160 --> 00:00:23,120
..para ajustar o brilho de sua tela.

8
00:00:24,080 --> 00:00:26,520
A experiência com tela sensível ao toque (touch screen) também foi melhorada..

9
00:00:26,600 --> 00:00:29,040
..ao selecionar e alterar texto.

10
00:00:30,760 --> 00:00:33,560
O aplicativo Arquivos recebeu bastante atenção neste ciclo.

11
00:00:34,320 --> 00:00:37,520
Há uma nova visão na barra lateral, deste modo locais remotos e internos..

12
00:00:37,520 --> 00:00:40,280
..são coletados em um lugar para a sua conveniência.

13
00:00:40,960 --> 00:00:42,960
O andamento é, agora, mostrado na barra de cabeçalho..

14
00:00:42,960 --> 00:00:44,960
..quando os arquivos estão sendo copiados.

15
00:00:45,400 --> 00:00:47,000
Há muitos outros aprimoramentos..

16
00:00:47,320 --> 00:00:49,320
..incluindo uma função de pesquisa mais refinada..

17
00:00:50,520 --> 00:00:52,520
..integração com o Google Drive..

18
00:00:52,520 --> 00:00:54,520
..e melhor gerenciamento de arquivos.

19
00:00:58,160 --> 00:01:01,400
O aplicativo Programas se tornou o primeiro a suportar a atualização de firmware de dispositivos..

20
00:01:01,400 --> 00:01:04,480
..através do novo serviço de "Linux Vendor Firmware" (Firmware de fornecedor Linux).

21
00:01:05,480 --> 00:01:08,200
O serviço permite que fornecedores enviem firmware..

22
00:01:08,200 --> 00:01:11,880
..e o aplicativo Programas do GNOME irá lhe permitir instalá-lo com o clique de um botão.

23
00:01:14,240 --> 00:01:16,720
Além disso, a comunidade GNOME..

24
00:01:16,720 --> 00:01:19,280
..melhorou uma grande variedade de aplicativos..

25
00:01:19,280 --> 00:01:20,760
..incluindo o aplicativo Agenda..

26
00:01:20,760 --> 00:01:23,120
..que agora vem com uma caixa de diálogo de gerenciamento..

27
00:01:23,120 --> 00:01:24,760
..para ajudá-lo(a) a gerenciar suas agendas.

28
00:01:25,400 --> 00:01:26,680
Polari..

29
00:01:26,680 --> 00:01:29,280
..com uma interface gráfica inicial bem melhor.

30
00:01:30,280 --> 00:01:31,560
Documentos do GNOME..

31
00:01:31,560 --> 00:01:35,360
..que está ostentando uma interface gráfica renovada para organizar seus documentos.

32
00:01:35,760 --> 00:01:36,640
E o Evince..

33
00:01:36,640 --> 00:01:39,440
.. que teve o suporte a anotações e vídeos melhorado.

34
00:01:43,080 --> 00:01:46,200
O GNOME 3.18 vem com uma versão em desenvolvimento do GNOME Tarefas..

35
00:01:46,200 --> 00:01:50,520
..um gerenciador pessoal de tarefas ajudando a você manter o controle das tarefas antes do tempo.

36
00:01:52,040 --> 00:01:55,120
As ferramentas de desenvolvimento do GNOME também melhoraram neste ciclo.

37
00:01:55,440 --> 00:01:57,040
Agora o Builder, por exemplo..

38
00:01:57,040 --> 00:01:58,960
..possui um minimapa de código-fonte..

39
00:01:59,000 --> 00:02:00,680
.. uma visão-geral de atalho..

40
00:02:00,680 --> 00:02:02,480
..e preenchimento automático de código Python.

41
00:02:03,280 --> 00:02:06,400
As diretrizes de interfaces humanas foram reestruturadas..

42
00:02:06,400 --> 00:02:08,400
..e inclui novos padrões de uso.

43
00:02:09,440 --> 00:02:12,840
Agora, a interface gráfica do aplicativo Registros do GNOME fornece uma melhor visão-geral..

44
00:02:12,920 --> 00:02:15,400
..e pode lhe mostrar registros das últimas cinco inicializações.

45
00:02:16,240 --> 00:02:18,320
O Boxes melhorou sua interface..

46
00:02:18,320 --> 00:02:19,680
..com uma nova visão de lista..

47
00:02:19,680 --> 00:02:21,960
..para usuários com várias máquinas virtuais.

48
00:02:22,280 --> 00:02:25,960
Finalmente, o GTK+ recebeu novos recursos de tipografia.

49
00:02:28,280 --> 00:02:30,600
O novo lançamento do GNOME está disponível para você..

50
00:02:30,680 --> 00:02:32,520
..como uma imagem "live" para experimentar agora.

51
00:02:33,600 --> 00:02:37,400
Muitas distribuições também irão incorporar o GNOME 3.18 em um futuro próximo.

52
00:02:39,360 --> 00:02:41,520
O GNOME é uma comunidade amigável..

53
00:02:41,520 --> 00:02:43,200
..que está tornando o software livre para todos.

54
00:02:43,960 --> 00:02:44,900
Empolgado(a)?

55
00:02:45,280 --> 00:02:48,500
Junte-se a nós para tornar o GNOME o ambiente de área de trabalho perfeito.
