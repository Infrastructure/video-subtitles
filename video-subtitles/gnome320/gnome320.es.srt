1
00:00:03,980 --> 00:00:05,900
GNOME es una comunidad mundial..

2
00:00:05,900 --> 00:00:09,140
..dedicada a crear un escritorio libre y de código abierto.

3
00:00:09,940 --> 00:00:13,420
La versión 3.20 de GNOME consta de las contribuciones de los 6 últimos meses..

4
00:00:13,420 --> 00:00:16,080
..de programadores, diseñadores y escritores.

5
00:00:19,460 --> 00:00:22,520
Como resultado, una docena de aplicaciones de GNOME incluyen características nuevas..

6
00:00:22,520 --> 00:00:24,200
..y una interfaz más atractiva.

7
00:00:39,620 --> 00:00:41,140
GNOME Fotos puede recortar sus fotos..

8
00:00:41,140 --> 00:00:43,140
..y aplicarles filtros.

9
00:00:45,160 --> 00:00:47,720
El centro de control ostenta un panel del ratón rediseñado.

10
00:00:50,840 --> 00:00:52,840
Software ofrece una integración mejorada..

11
00:00:52,840 --> 00:00:54,840
..de aplicaciones aisladas y actualizaciones del S.O.

12
00:00:56,680 --> 00:01:00,340
Además, la mayoría de las aplicaciones incluyen una ventana de atajos del teclado.

13
00:01:04,260 --> 00:01:07,200
Los desarrolladores pueden esperar un GNOME Builder mejorado..

14
00:01:08,740 --> 00:01:11,080
..que incluye un construcción de aplicaciones xdg..

15
00:01:11,900 --> 00:01:13,460
..y plantillas de proyectos.

16
00:01:17,800 --> 00:01:19,780
Se ha estabilizado el soporte de temas en GTK+..

17
00:01:19,780 --> 00:01:21,260
..al usar elementos CSS.

18
00:01:30,180 --> 00:01:33,460
GNOME 3.20 se incluirá pronto en muchas distribuciones.

19
00:01:35,760 --> 00:01:38,800
La comunidad GNOME es amigable y abierta a todo el mundo.

20
00:01:39,400 --> 00:01:42,680
Construya un escritorio libre mejor involucrándose hoy mismo.
