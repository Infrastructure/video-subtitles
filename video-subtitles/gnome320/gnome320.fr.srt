1
00:00:03,980 --> 00:00:05,900
GNOME est une communauté mondiale

2
00:00:05,900 --> 00:00:09,140
consacrée à la création d'un environnement de bureau libre.

3
00:00:09,940 --> 00:00:13,420
GNOME 3.20 consiste en 6 mois de contributions

4
00:00:13,420 --> 00:00:16,080
des codeurs, designers et rédacteurs.

5
00:00:19,460 --> 00:00:22,520
Le résultat est une douzaine d'applications avec de nouvelles fonctionnalités

6
00:00:22,520 --> 00:00:24,200
et une interface plus attrayante.

7
00:00:39,620 --> 00:00:41,140
GNOME Photos peut recadrer vos photos

8
00:00:41,140 --> 00:00:43,140
et leur appliquer des filtres.

9
00:00:45,160 --> 00:00:47,720
Le panneau des paramètres de la souris a été refondu.

10
00:00:50,840 --> 00:00:52,840
Logiciels fournit une intégration transparente

11
00:00:52,840 --> 00:00:54,840
des applications en bacs à sable et des mises à niveau du système.

12
00:00:56,680 --> 00:01:00,340
De plus, la plupart des applications a désormais une fenêtre pour les raccourcis clavier.

13
00:01:04,260 --> 00:01:07,200
Les développeurs découvriront un Builder amélioré,

14
00:01:08,740 --> 00:01:11,080
qui propose maintenant de construire avec xdg-app

15
00:01:11,900 --> 00:01:13,460
et des modèles de projets.

16
00:01:17,800 --> 00:01:19,780
GTK+ a stabilisé sa prise en charge des thèmes

17
00:01:19,780 --> 00:01:21,260
avec le passage aux éléments CSS.

18
00:01:30,180 --> 00:01:33,460
GNOME 3.20 sera bientôt disponible dans de nombreuses distributions.

19
00:01:35,760 --> 00:01:38,800
La communauté GNOME est accueillante et ouverte à tous.

20
00:01:39,400 --> 00:01:42,680
Construisez un meilleur environnement de bureau avec nous en vous impliquant aujourd'hui.
