1
00:00:03,980 --> 00:00:05,900
GNOME is a worldwide community..

2
00:00:05,900 --> 00:00:09,140
..dedicated to create a free and open source desktop.

3
00:00:09,940 --> 00:00:13,420
The GNOME 3.20 release consists of the last 6 months of contributions..

4
00:00:13,420 --> 00:00:16,080
..from coders, designers and writers.

5
00:00:19,460 --> 00:00:22,520
As a result, a dozen of GNOME's applications comes with new features..

6
00:00:22,520 --> 00:00:24,200
..and a more attractive interface.

7
00:00:39,620 --> 00:00:41,140
GNOME Photos can crop your photos..

8
00:00:41,140 --> 00:00:43,140
..and apply filters to them.

9
00:00:45,160 --> 00:00:47,720
The control center sports a revamped mouse panel.

10
00:00:50,840 --> 00:00:52,840
Software provides a seamless integration..

11
00:00:52,840 --> 00:00:54,840
..of sandboxed applications and OS upgrades.

12
00:00:56,680 --> 00:01:00,340
And furthermore, most applications now come with a keyboard shortcut window.

13
00:01:04,260 --> 00:01:07,200
Developers can look forward to an improved GNOME Builder,..

14
00:01:08,740 --> 00:01:11,080
..which now comes with xdg-app building..

15
00:01:11,900 --> 00:01:13,460
..and project templates.

16
00:01:17,800 --> 00:01:19,780
GTK+ has stabilized its theming support..

17
00:01:19,780 --> 00:01:21,260
..with the move to CSS elements.

18
00:01:30,180 --> 00:01:33,460
GNOME 3.20 will be provided by many distributions soon.

19
00:01:35,760 --> 00:01:38,800
The GNOME community is friendly and open for anyone.

20
00:01:39,400 --> 00:01:42,680
Build a better free desktop with us by getting involved today.

