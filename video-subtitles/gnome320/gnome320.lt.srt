1
00:00:03,980 --> 00:00:05,900
GNOME yra pasaulinė bendruomenė,..

2
00:00:05,900 --> 00:00:09,140
..pasišventusi kurti laisvą, atviro kodo darbalaukį.

3
00:00:09,940 --> 00:00:13,420
GNOME 3.20 laida susideda iš pagalbos įnašų, kuriuos per paskutinius 6 mėnesius..

4
00:00:13,420 --> 00:00:16,080
..pateikė programuotojai, dizaineriai ir autoriai.

5
00:00:19,460 --> 00:00:22,520
To rezultatas - dešimtis GNOME programų pasirodo su naujomis ypatybėmis..

6
00:00:22,520 --> 00:00:24,200
..ir patrauklesne sąsaja.

7
00:00:39,620 --> 00:00:41,140
GNOME Nuotraukos gali apkirpti jūsų nuotraukas..

8
00:00:41,140 --> 00:00:43,140
..ir pritaikyti joms filtrus.

9
00:00:45,160 --> 00:00:47,720
Valdymo centre puikuojasi pataisytas pelės skydelis.

10
00:00:50,840 --> 00:00:52,840
Programinė įranga pateikia vientisą izoliuotų..

11
00:00:52,840 --> 00:00:54,840
..programų ir OS atnaujinimų integraciją.

12
00:00:56,680 --> 00:01:00,340
Be to, daugelyje programų dabar yra prieinamas klaviatūros trumpinių langas.

13
00:01:04,260 --> 00:01:07,200
Kūrėjai gali tikėtis patobulinto GNOME Kūrėjo,..

14
00:01:08,740 --> 00:01:11,080
..kuris dabar pristatomas su xdg-app kūrimu..

15
00:01:11,900 --> 00:01:13,460
..ir projektų šablonais.

16
00:01:17,800 --> 00:01:19,780
GTK+, perėjimu prie CSS elementų,..

17
00:01:19,780 --> 00:01:21,260
..stabilizavo savo temų palaikymą.

18
00:01:30,180 --> 00:01:33,460
Daugelis distribucijų greitu metu pateiks GNOME 3.20.

19
00:01:35,760 --> 00:01:38,800
GNOME yra draugiška ir visiems atvira bendruomenė.

20
00:01:39,400 --> 00:01:42,680
Įsitraukdami šiandien, kartu su mumis kurkite geresnį darbalaukį.
