1
00:00:03,980 --> 00:00:05,900
GNOME jest światową społecznością

2
00:00:05,900 --> 00:00:09,140
tworzącą wolne środowisko pulpitu.

3
00:00:09,940 --> 00:00:13,420
Wydanie GNOME 3.20 to efekt 6 miesięcy pracy

4
00:00:13,420 --> 00:00:16,080
programistów, projektantów i tłumaczy.

5
00:00:19,460 --> 00:00:22,520
Dzięki temu wiele programów GNOME zawiera nowe funkcje

6
00:00:22,520 --> 00:00:24,200
i bardziej atrakcyjny interfejs.

7
00:00:39,620 --> 00:00:41,140
Menedżer zdjęć GNOME może przycinać zdjęcia

8
00:00:41,140 --> 00:00:43,140
i stosować na nich filtry.

9
00:00:45,160 --> 00:00:47,720
Centrum sterowania ma zreorganizowany panel myszy.

10
00:00:50,840 --> 00:00:52,840
Menedżer oprogramowania bezproblemowo integruje się

11
00:00:52,840 --> 00:00:54,840
z ograniczanymi programami i aktualizacjami systemu.

12
00:00:56,680 --> 00:01:00,340
Co więcej, większość programów ma teraz okno skrótów klawiszowych.

13
00:01:04,260 --> 00:01:07,200
Programiści ucieszą się z ulepszonego programu GNOME Builder

14
00:01:08,740 --> 00:01:11,080
obsługującego teraz budowanie pakietów xdg-app

15
00:01:11,900 --> 00:01:13,460
i szablony projektów.

16
00:01:17,800 --> 00:01:19,780
Obsługa motywów w bibliotece GTK+ została ustabilizowana

17
00:01:19,780 --> 00:01:21,260
za pomocą przejścia do elementów CSS.

18
00:01:30,180 --> 00:01:33,460
GNOME 3.20 niedługo będzie dostępne w wielu dystrybucjach.

19
00:01:35,760 --> 00:01:38,800
Społeczność GNOME jest przyjazna i otwarta na wszystkich.

20
00:01:39,400 --> 00:01:42,680
Dołącz do nas w budowaniu lepszego wolnego środowiska pulpitu.
