1
00:00:03,980 --> 00:00:05,900
GNOME là một cộng đồng rộng lớn..

2
00:00:05,900 --> 00:00:09,140
..tận tụy để tạo nên môi trường máy tính để bàn tự do và mở.

3
00:00:09,940 --> 00:00:13,420
Bản phát hành GNOME 3.20 gồm có công sức đóng góp 6 tháng của..

4
00:00:13,420 --> 00:00:16,080
..những nhà lập trình, thiết kế và viết lách.

5
00:00:19,460 --> 00:00:22,520
Kết quả là hàng tá các ứng dụng xuất hiện với những tính năng mới..

6
00:00:22,520 --> 00:00:24,200
..và một giao diện hấp dẫn hơn.

7
00:00:39,620 --> 00:00:41,140
GNOME Photos có thể xén các ảnh cho bạn..

8
00:00:41,140 --> 00:00:43,140
..và áp dụng các bộ lọc cho chúng.

9
00:00:45,160 --> 00:00:47,720
Trung tâm điều khiển trưng diện bảng điều khiển con chuột có sửa chữa.

10
00:00:50,840 --> 00:00:52,840
Software cung cấp một môi trường thống nhất..

11
00:00:52,840 --> 00:00:54,840
..cho các ứng dụng sandboxed và nâng cấp OS.

12
00:00:56,680 --> 00:01:00,340
Hơn thế nữa, phần lớn các ứng dùng giờ sẽ đến cùng với cửa sổ phím tắt.

13
00:01:04,260 --> 00:01:07,200
Các nhà phát triển có thể hướng đến việc cải tiến GNOME Builder,..

14
00:01:08,740 --> 00:01:11,080
..cái mà giờ đến cùng với bộ xây dựng xdg-app..

15
00:01:11,900 --> 00:01:13,460
..và các mẫu dự án.

16
00:01:17,800 --> 00:01:19,780
GTK+ đã hỗ trợ ổn định chủ đề mà nó hỗ trợ..

17
00:01:19,780 --> 00:01:21,260
..với việc chuyển đến dùng phần tử CSS.

18
00:01:30,180 --> 00:01:33,460
GNOME 3.20 sẽ được các bản phân phối cung cấp sớm thôi.

19
00:01:35,760 --> 00:01:38,800
Cộng đồng GNOME thân thiện và mở cho mọi người.

20
00:01:39,400 --> 00:01:42,680
Hãy tham gia cùng chúng tôi ngay hôm nay để xây dựng một môi trường máy tính để bàn tốt hơn.
