1
00:00:03,980 --> 00:00:05,900
GNOME är en världsomspännande gemenskap…

2
00:00:05,900 --> 00:00:09,140
…driven av målet att skapa en fri skrivbordsmiljö som består av öppen källkod.

3
00:00:09,940 --> 00:00:13,420
Utgåvan GNOME 3.20 består av de senaste sex månadernas bidrag…

4
00:00:13,420 --> 00:00:16,080
…från kodare, formgivare och skribenter.

5
00:00:19,460 --> 00:00:22,520
Som ett resultat kommer ett dussin av GNOME:s program med nya funktioner…

6
00:00:22,520 --> 00:00:24,200
…och ett mer attraktivt gränssnitt.

7
00:00:39,620 --> 00:00:41,140
GNOME Foton kan beskära dina bilder…

8
00:00:41,140 --> 00:00:43,140
…och tillämpa filter på dem.

9
00:00:45,160 --> 00:00:47,720
Kontrollpanelen har en omarbetad muspanel.

10
00:00:50,840 --> 00:00:52,840
Programvara tillhandahåller sömlös integration…

11
00:00:52,840 --> 00:00:54,840
…av program i sandlåda och OS-uppgraderingar.

12
00:00:56,680 --> 00:01:00,340
Vidare kommer de flesta programmen nu med ett fönster för tangentbordsgenvägar.

13
00:01:04,260 --> 00:01:07,200
Utvecklare kan se fram emot ett förbättrat GNOME Builder…

14
00:01:08,740 --> 00:01:11,080
…som nu kommer med xdg-app-byggande…

15
00:01:11,900 --> 00:01:13,460
…och projektmallar.

16
00:01:17,800 --> 00:01:19,780
GTK+ har stabiliserat sitt stöd för teman…

17
00:01:19,780 --> 00:01:21,260
…med flytten till CSS-element.

18
00:01:30,180 --> 00:01:33,460
GNOME 3.20 kommer snart att tillhandahållas av många distributioner.

19
00:01:35,760 --> 00:01:38,800
GNOME-gemenskapen är vänlig och öppen för alla.

20
00:01:39,400 --> 00:01:42,680
Bygg en bättre fri skrivbordsmiljö med oss genom att engagera dig idag.
