1
00:00:03,980 --> 00:00:05,900
A GNOME egy egész világon átívelő közösség…

2
00:00:05,900 --> 00:00:09,140
…melynek célja egy szabad és nyílt forráskódú asztali környezet készítése.

3
00:00:09,940 --> 00:00:13,420
A GNOME 3.20-as kiadása az elmúlt 6 hónap közreműködéseiből tevődik össze…

4
00:00:13,420 --> 00:00:16,080
…programozóktól, dizájnerektől és íróktól.

5
00:00:19,460 --> 00:00:22,520
Ennek eredményeként egy tucat GNOME alkalmazás új funkciókkal érkezik…

6
00:00:22,520 --> 00:00:24,200
…és még vonzóbb felülettel.

7
00:00:39,620 --> 00:00:41,140
A GNOME Fényképek képes levágni a fényképeket…

8
00:00:41,140 --> 00:00:43,140
…és szűrőket alkalmazni rájuk.

9
00:00:45,160 --> 00:00:47,720
A vezérlőközpont egy átalakított egér panelt kapott.

10
00:00:50,840 --> 00:00:52,840
A Szoftverek zökkenőmentes integrációs biztosít…

11
00:00:52,840 --> 00:00:54,840
…homokozóban futó alkalmazásokkal és OS frissítésekkel.

12
00:00:56,680 --> 00:01:00,340
Továbbá a legtöbb alkalmazás most már gyorsbillentyűablakkal érkezik.

13
00:01:04,260 --> 00:01:07,200
A fejlesztők számíthatnak a továbbfejlesztett GNOME Építőre…

14
00:01:08,740 --> 00:01:11,080
…amely most xdg-app építéssel érkezik…

15
00:01:11,900 --> 00:01:13,460
…és projektsablonokkal.

16
00:01:17,800 --> 00:01:19,780
A GTK+ stabilizálta a tématámogatást…

17
00:01:19,780 --> 00:01:21,260
…a CSS elemekre történő átállással.

18
00:01:30,180 --> 00:01:33,460
A GNOME 3.20-at számos disztribúció fogja hamarosan szállítani.

19
00:01:35,760 --> 00:01:38,800
A GNOME közösség barátságos és mindenki számára nyitott.

20
00:01:39,400 --> 00:01:42,680
Építsen velünk egy szabad asztali környezetet, legyen még ma közreműködő.
