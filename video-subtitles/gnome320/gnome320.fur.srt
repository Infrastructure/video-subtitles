1
00:00:03,980 --> 00:00:05,900
GNOME e je une comunitât mondiâl..

2
00:00:05,900 --> 00:00:09,140
..dedicade a creâ un scritori libar e open source.

3
00:00:09,940 --> 00:00:13,420
La version 3.20 di GNOME e consist di almancul 6 mês di contribûts..

4
00:00:13,420 --> 00:00:16,080
..di programadôrs, disegnadôrs e scritôrs.

5
00:00:19,460 --> 00:00:22,520
Come risultât, une dozene di aplicazions di GNOME a rivin cun gnovis funzionalitâts..

6
00:00:22,520 --> 00:00:24,200
..e une interface plui biele.

7
00:00:39,620 --> 00:00:41,140
Fotos di GNOME al pues rifilâ lis tôs fotos..

8
00:00:41,140 --> 00:00:43,140
..e aplicâ lôr filtris.

9
00:00:45,160 --> 00:00:47,720
Il centri di control al met in mostre un gnûf panel dal mouse.

10
00:00:50,840 --> 00:00:52,840
Software al furnìs une perfete integrazion..

11
00:00:52,840 --> 00:00:54,840
..di aplicazions in modalitât sandbox e inzornaments OS.

12
00:00:56,680 --> 00:01:00,340
E in plui, la plui part des aplicazions cumò a àn un barcon pes scurtis di tastiere.

13
00:01:04,260 --> 00:01:07,200
I svilupadôrs a puedin viodi cun interès a un GNOME Builder miorât,..

14
00:01:08,740 --> 00:01:11,080
..che cumò al ven furnît cun lis costruzions xdg-app..

15
00:01:11,900 --> 00:01:13,460
..e modei di progjet.

16
00:01:17,800 --> 00:01:19,780
GTK+ al à stabilizât il so supuart pai temis..

17
00:01:19,780 --> 00:01:21,260
..cul passaç di elements CSS.

18
00:01:30,180 --> 00:01:33,460
GNOME 3.20 al vegnarà furnît, chi di pôc, di tantis distribuzions.

19
00:01:35,760 --> 00:01:38,800
La comunitât di GNOME e je ben disponude e vierte a ducj.

20
00:01:39,400 --> 00:01:42,680
Costruìs e miore un scritori libar cun nô vignint coinvolt vuê.
