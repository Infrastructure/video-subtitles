1
00:00:03,980 --> 00:00:05,900
GNOME je celosvětové společenství lidí, kteří se věnují…

2
00:00:05,900 --> 00:00:09,140
…vytváření svobodného uživatelského prostředí s otevřeným kódem.

3
00:00:09,940 --> 00:00:13,420
GNOME 3.20 je výsledkem příspěvků za posledních 6 měsíců…

4
00:00:13,420 --> 00:00:16,080
…od programátorů, grafiků a dokumentátorů.

5
00:00:19,460 --> 00:00:22,520
Ve výsledku získalo tucet aplikací GNOME nové funkce…

6
00:00:22,520 --> 00:00:24,200
…a mnohem přitažlivější uživatelské rozhraní.

7
00:00:39,620 --> 00:00:41,140
Fotky GNOME umí ořezávat fotky…

8
00:00:41,140 --> 00:00:43,140
…a použít u nich různé filtry.

9
00:00:45,160 --> 00:00:47,720
Řídící centrum se honosí přepracovaným panelem pro myš.

10
00:00:50,840 --> 00:00:52,840
Software poskytuje dokonale zapadající integraci…

11
00:00:52,840 --> 00:00:54,840
…aplikací uzavřených na pískovišti a povýšení operačního systému.

12
00:00:56,680 --> 00:01:00,340
A navíc má teď většina aplikací okno se seznamem klávesových zkratek.

13
00:01:04,260 --> 00:01:07,200
Vývojáři se mohou těšit na vylepšený Builder GNOME, …

14
00:01:08,740 --> 00:01:11,080
…který nyní umí sestavovat xdg-app…

15
00:01:11,900 --> 00:01:13,460
…a má projektové šablony.

16
00:01:17,800 --> 00:01:19,780
GTK+ má konečně stabilní podporu svých motivů, …

17
00:01:19,780 --> 00:01:21,260
…která se přesunula do prvků CSS.

18
00:01:30,180 --> 00:01:33,460
V dohledné době se GNOME 3.20 dostane do řady distribucí.

19
00:01:35,760 --> 00:01:38,800
Společenství GNOME je přátelské a otevřené pro všechny.

20
00:01:39,400 --> 00:01:42,680
Build a better free desktop with us by getting involved today.
