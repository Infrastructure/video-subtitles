1
00:00:03,980 --> 00:00:05,900
Gnomov projekat je svetska zajednica..

2
00:00:05,900 --> 00:00:09,140
..koja želi da napravi slobodnu radnu površ otvorenog kȏda.

3
00:00:09,940 --> 00:00:13,420
Gnom 3.20 je rezultat šestomesečnog rada dobrovoljnog doprinosa..

4
00:00:13,420 --> 00:00:16,080
..programera, dizajnera i pisaca.

5
00:00:19,460 --> 00:00:22,520
Kao rezultat, tuce Gnomovih programa dolazi sa novim funkcijama..

6
00:00:22,520 --> 00:00:24,200
..i sa privlačnijim sučeljem.

7
00:00:39,620 --> 00:00:41,140
Gnomove fotografije mogu da opseču fotografije..

8
00:00:41,140 --> 00:00:43,140
..i da primene filtere na njima.

9
00:00:45,160 --> 00:00:47,720
Upravljački centar poseduje obnovljeni panel miša.

10
00:00:50,840 --> 00:00:52,840
Programi pruža jednostavnu integraciju..

11
00:00:52,840 --> 00:00:54,840
..programa zaštićenog okruženja i nadogradnji operativnog sistema.

12
00:00:56,680 --> 00:01:00,340
Uz to, većina programa sada dolazi sa prozorom prečica sa tastature.

13
00:01:04,260 --> 00:01:07,200
Programere će obradovati poboljšani Gnomov graditelj,..

14
00:01:08,740 --> 00:01:11,080
..koji sada dolazi sa „xdg-app“ izgradnjom..

15
00:01:11,900 --> 00:01:13,460
..i šablonima projekata.

16
00:01:17,800 --> 00:01:19,780
Gtk+ je stabilizovao podršku tema..

17
00:01:19,780 --> 00:01:21,260
..prelaskom na CSS elemente.

18
00:01:30,180 --> 00:01:33,460
Mnoge distribucije će uskoro početi da dostavljaju Gnom 3.20.

19
00:01:35,760 --> 00:01:38,800
Gnomova zajednica je prijateljska i otvorena za svakoga.

20
00:01:39,400 --> 00:01:42,680
Pridružite nam se već danas da bismo zajedno izgradili bolju slobodnu radnu površ.
