1
00:00:03,980 --> 00:00:05,900
O GNOME é uma comunidade mundial..

2
00:00:05,900 --> 00:00:09,140
..dedicada a criar um ambiente livre e de código aberto.

3
00:00:09,940 --> 00:00:13,420
O GNOME 3.20 é o resultado dos últimos 6 meses de contribuições..

4
00:00:13,420 --> 00:00:16,080
..de programadores, designers e escritores.

5
00:00:19,460 --> 00:00:22,520
Como resultado, diversos aplicativos do GNOME vêm com novos recursos..

6
00:00:22,520 --> 00:00:24,200
..e uma interface mais atrativa.

7
00:00:39,620 --> 00:00:41,140
GNOME Fotos pode cortar suas fotos..

8
00:00:41,140 --> 00:00:43,140
..e aplicar filtros nelas.

9
00:00:45,160 --> 00:00:47,720
O centro de controle ostenta um painel de mouse renovado.

10
00:00:50,840 --> 00:00:52,840
Softwares fornece uma integração perfeita..

11
00:00:52,840 --> 00:00:54,840
..de aplicativos seguros (em sandbox) e atualizações do SO.

12
00:00:56,680 --> 00:01:00,340
Além disso, a maioria dos aplicativos agora contém uma janela de atalhos.

13
00:01:04,260 --> 00:01:07,200
Desenvolvedores podem esperar um GNOME Builder melhorado,..

14
00:01:08,740 --> 00:01:11,080
..que agora vem com compilação com xdg-app..

15
00:01:11,900 --> 00:01:13,460
..e modelos de projetos.

16
00:01:17,800 --> 00:01:19,780
GTK+ estabilizou seu suporte a temas..

17
00:01:19,780 --> 00:01:21,260
..com a migração para elementos CSS.

18
00:01:30,180 --> 00:01:33,460
GNOME 3.20 será fornecido em várias distribuições em breve.

19
00:01:35,760 --> 00:01:38,800
A comunidade GNOME é amigável e aberta para todos.

20
00:01:39,400 --> 00:01:42,680
Construa um ambiente livre melhor conosco, contribuindo desde hoje.
