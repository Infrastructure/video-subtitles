0
00:00:04,875 --> 00:00:07,125
GNOME is a large project..

1
00:00:07,125 --> 00:00:10,333
..creating a modern and elegant desktop experience.

2
00:00:11,292 --> 00:00:14,125
Behind GNOME is a passionate community of contributors..

3
00:00:14,125 --> 00:00:16,292
..who all work together to improve the performance,..

4
00:00:16,292 --> 00:00:18,708
..design and features of the GNOME desktop.

5
00:00:19,083 --> 00:00:23,292
Their collective efforts bring many exciting changes in the 3.30 release.

6
00:00:24,250 --> 00:00:28,667
This cycle, GNOME Shell has had a serious focus on optimizing performance.

7
00:00:28,667 --> 00:00:33,083
It should now be significantly easier to run on lower powered devices.

8
00:00:33,333 --> 00:00:36,667
Control Center continues to bring even more improvements,..

9
00:00:36,667 --> 00:00:41,458
..with GNOME 3.30 adding compatibility and control for Thunderbolt 3 devices.

10
00:00:42,250 --> 00:00:46,250
Development on Files continues with a unified path and search bar..

11
00:00:46,250 --> 00:00:48,917
..and now, when resizing the window..

12
00:00:48,917 --> 00:00:51,750
..the icons will intelligently adjust their distance from one another..

13
00:00:51,750 --> 00:00:53,583
..to fill the available space.

14
00:00:55,083 --> 00:00:58,417
Many other applications have had updates this cycle.

15
00:00:58,417 --> 00:01:00,875
Web has a new reader mode,..

16
00:01:03,125 --> 00:01:06,583
..Notes has improved note style and zoom,..

17
00:01:09,292 --> 00:01:12,833
..Boxes has gained FreeRDP support,..

18
00:01:15,333 --> 00:01:18,292
..and Fractal, the GNOME Matrix messaging client,..

19
00:01:18,292 --> 00:01:22,208
..has improved room discovery, performance, and stability.

20
00:01:26,292 --> 00:01:30,792
With GNOME 3.30 we are also debuting a brand-new app, Podcasts..

21
00:01:30,792 --> 00:01:35,292
..which is a fast and easy-to-use podcast client for the GNOME desktop.

22
00:01:36,833 --> 00:01:40,000
Developers can look for quality of life improvements to Builder.

23
00:01:40,000 --> 00:01:43,333
A new autocompletion engine provides more intelligent suggestions..

24
00:01:43,333 --> 00:01:45,083
..with much better performance..

25
00:01:45,083 --> 00:01:49,042
..and new interactive code tooltips show you beautifully formatted documentation..

26
00:01:49,042 --> 00:01:51,375
..in real-time while you code.

27
00:01:53,667 --> 00:01:55,375
GNOME is a world-wide project,..

28
00:01:55,375 --> 00:01:57,917
..and it is thanks to our passionate contributors and donors..

29
00:01:57,917 --> 00:02:01,917
..that we can keep providing quality, free-software to users around the world.

30
00:02:02,250 --> 00:02:04,417
With the support from our Friends of GNOME,..

31
00:02:04,417 --> 00:02:08,333
..we have held many events, summits, and hackfests over the past 6 months,..

32
00:02:08,333 --> 00:02:10,542
..including our largest GUADEC in recent years,..

33
00:02:10,542 --> 00:02:13,625
.. which took place this year in Almería, Spain.

34
00:02:14,083 --> 00:02:16,583
Are you interested in contributing code, design..

35
00:02:16,583 --> 00:02:18,708
..or translations for our next release?

36
00:02:18,708 --> 00:02:20,083
Get involved today!

37
00:02:20,083 --> 00:02:22,375
Read more at GNOME.org

