1
00:00:02,500 --> 00:00:04,500
GNOME Delivers a full desktop experience

2
00:00:06,800 --> 00:00:08,000
Part of this means making it easy for users..

3
00:00:08,200 --> 00:00:10,000
..to explore new features and workflows.

4
00:00:11,800 --> 00:00:14,800
This is where GNOME Help comes into the picture.

5
00:00:16,000 --> 00:00:17,900
Need to know the keyboard shortcuts of gedit?

6
00:00:19,000 --> 00:00:22,300
GNOME Help lets you explore them easily through its interface.

7
00:00:22,800 --> 00:00:24,600
Want to know what Documents has to offer?

8
00:00:25,000 --> 00:00:27,700
Press F1 and you can get an overview of..

9
00:00:27,700 --> 00:00:29,200
..Documents' features at a glance.

10
00:00:30,200 --> 00:00:32,200
GNOME Help is your offline assistant..

11
00:00:32,300 --> 00:00:35,200
..ready to lend a hand with any application adventure you may take.

12
00:00:36,300 --> 00:00:38,200
The documentation is there to help you speed up..

13
00:00:39,100 --> 00:00:40,700
..and get work done.

14
00:00:41,600 --> 00:00:44,200
Similarly, the GNOME project aims to provide good documentation..

15
00:00:44,200 --> 00:00:46,700
..for developers and administrators.

16
00:00:47,000 --> 00:00:50,100
For developers, help is provided through Devhelp..

17
00:00:50,100 --> 00:00:52,600
..and online at the GNOME Developer Center.

18
00:00:53,000 --> 00:00:55,900
Combined you have a library of knowledge and tools..

19
00:00:55,900 --> 00:00:57,900
..to get you up to date with the latest happenings..

20
00:00:57,900 --> 00:01:00,000
..in everything from glib to gnome-shell..

21
00:01:00,000 --> 00:01:03,000
..in terms of source code, API and plug-in development.

22
00:01:05,000 --> 00:01:07,200
For system administrators and tinkerers..

23
00:01:07,200 --> 00:01:10,000
..we provide the System Administration Guide online..

24
00:01:10,000 --> 00:01:12,000
at help.gnome.org.

25
00:01:13,400 --> 00:01:14,800
This guide aims at addressing your needs..

26
00:01:14,800 --> 00:01:18,000
..whether it be managing the configuration of several computers from one place..

27
00:01:18,000 --> 00:01:21,200
..or playing with the logo on the login screen.

28
00:01:22,100 --> 00:01:26,200
Together, Help, Developer Center and the Sysadmin Guide..

29
00:01:26,100 --> 00:01:27,500
..makes up GNOMEs documentation.

30
00:01:27,500 --> 00:01:31,500
Of course, we are always looking to make the documentation better.

31
00:01:32,500 --> 00:01:33,500
Got an idea, found a typo..

32
00:01:33,500 --> 00:01:35,000
..or just want to find a way to help?

33
00:01:36,000 --> 00:01:38,500
File a bug at bugzilla.gnome.org..

34
00:01:38,500 --> 00:01:40,500
..against "gnome-user-docs".

35
00:01:42,000 --> 00:01:44,500
Or come hangout at in the #docs channel..

36
00:01:44,500 --> 00:01:46,800
..at irc.gnome.org

37
00:01:48,500 --> 00:01:51,800
This is Karen Sandler, thank you for using GNOME.
