﻿1
00:00:03,229 --> 00:00:10,089
'Open source' is not a dirty word any more. It used to be a dirty word, a scary one.

2
00:00:10,089 --> 00:00:16,259
For example, in e-government applications and public administrations open source was

3
00:00:16,260 --> 00:00:22,380
something to be used at home, as a hobby. Nobody would dare to push for an open source

4
00:00:22,379 --> 00:00:27,549
solution in a public organisation. "Who will offer us technical support, and

5
00:00:27,550 --> 00:00:32,689
what if we are sued for infringing someone's IP if we use this?"

6
00:00:32,689 --> 00:00:38,980
Those were the sort of issues that discouraged officials from selecting software on merit.

7
00:00:38,979 --> 00:00:45,808
We do not yet have a true level playing field, but thankfully attitudes are changing.

8
00:00:45,808 --> 00:00:52,308
One example is the European Commission's OSOR project. This project raises awareness and

9
00:00:52,308 --> 00:00:59,250
stimulates reuse of successful open source solutions, across European public administrations.

10
00:00:59,250 --> 00:01:06,250
And another example is EUPL, the EU open source license, which, under an EU legal framework,

11
00:01:07,500 --> 00:01:15,650
allows for easy licensing of open source software. Today many large organisations across Europe,

12
00:01:15,650 --> 00:01:22,300
such as the Munich City Council, use solutions like Linux. And the UK government has been

13
00:01:22,299 --> 00:01:28,769
pushing open source solutions since 2009, with many government departments now using

14
00:01:28,769 --> 00:01:33,989
it as a component. And from what I hear, such bodies are pleased

15
00:01:33,989 --> 00:01:39,799
with the results. The reason is not only good value for money, which is critical in today's

16
00:01:39,799 --> 00:01:46,799
financial situation, but also more choice. There is lower dependency on certain vendors,

17
00:01:47,480 --> 00:01:53,340
and lower switching costs. Things are changing, also in the private sector.

18
00:01:53,340 --> 00:01:59,520
Now large companies declare, proudly, that they are not only using open source software,

19
00:01:59,519 --> 00:02:06,259
but contributing to it. And that means that many important open source projects are in

20
00:02:06,260 --> 00:02:13,079
fact backed by businesses who are investing in it. They make good returns. And that is

21
00:02:13,079 --> 00:02:17,790
going to continue as a major boost for the open source movement.

22
00:02:17,789 --> 00:02:24,789
According to data from open source providers, like RedHat, the top three countries for open

23
00:02:24,818 --> 00:02:31,818
source activity in the EU are France, Spain and Germany. And with such big countries in

24
00:02:32,049 --> 00:02:36,248
the lead, the momentum for open source is set to keep on growing.

25
00:02:36,248 --> 00:02:43,900
Indeed, open source is appearing everywhere: in consumer products, in databases, in business

26
00:02:43,900 --> 00:02:49,930
software, in games and as a component of services delivered across the internet.

27
00:02:49,930 --> 00:02:54,519
And large initiatives like NOiV, the 'Nederland Open in Verbinding' (the Netherlands Openly

28
00:02:54,519 --> 00:02:59,980
in Connection), from my own country, the Netherlands, are helping to make it mainstream

29
00:02:59,979 --> 00:03:05,878
also in public administration. The European Commission has done a lot to

30
00:03:05,878 --> 00:03:13,840
encourage this trend. One example is the European Interoperability Framework, which aims at

31
00:03:13,840 --> 00:03:20,840
interoperability in and between public administrations. And now the digital agenda for Europe, is

32
00:03:20,989 --> 00:03:27,989
raising the stakes. Here we are aiming at a more strategic approach to interoperability

33
00:03:28,528 --> 00:03:33,919
and standards, and emphasising the important link to public procurement.

34
00:03:33,919 --> 00:03:39,619
That can really change the way open source is seen in public administrations and offer

35
00:03:39,620 --> 00:03:44,459
a lot of potential to small and medium sized providers.

36
00:03:44,459 --> 00:03:50,438
You have an important role in shaping Europe's digital future. Governments cannot simply

37
00:03:50,438 --> 00:03:57,438
announce and deliver the digital future. It must come out of an organic and shared responsibility.

38
00:03:57,979 --> 00:04:05,598
And I want to build a broad movement for digital action. At the EU we can bring people together,

39
00:04:05,598 --> 00:04:11,278
help get rid of obstacles, and occasionally give funding to help in research and development.

40
00:04:11,278 --> 00:04:18,278
But the real difference is made by people and communities, like the open source movement.

